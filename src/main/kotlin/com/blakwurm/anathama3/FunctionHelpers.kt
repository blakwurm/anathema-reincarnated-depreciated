package com.blakwurm.anathama3

/**
 * Created by Alex on 5/2/2016.
 */
data class OperationResult<T>(val wasSuccessful: Boolean = true, val message: String = "No message specified", val error: Exception = Exception("No error"), val result: T){
    infix fun saying(str: String) = OperationResult(this.wasSuccessful, str, Exception(), this.result)
    infix fun because(e: Exception) = OperationResult(this.wasSuccessful, this.message, e, this.result)
}
infix fun <T> Boolean.result(result: T) : OperationResult<T> = OperationResult(wasSuccessful= this, result = result)
