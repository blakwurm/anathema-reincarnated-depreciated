package com.blakwurm.anathama3.character

import com.blakwurm.anathama3.firstCap
import com.blakwurm.anathama3.getDotNotation
import com.blakwurm.anathama3.wrapAt
import org.apache.commons.lang3.text.WordUtils
import java.util.*

/**
 * A snapshot of a given mortal's state. Contained within exalt types to provide a core. Used by UI logic to
 * determine what values to begin with, and to export data when saving.
 */
data final class Mortal (
        /*
        Things that are on all character sheets:
        -Attributes, Anima, -Name, -Player Name, Caste/Aspect,
        -Abilities, -Merits, Weapons and Attacks,
        -Intimacies, Armor, Derived Values, Soak,
        Resolve, Guile, Charms.
         */
    val name: String,
    val player: String,

    val attributes: Map<Attribute, Int>,
    val abilities: Map<Ability, Int>,

    val merits: List<Merit>,

    val sorcerer: Sorcerer,

    val intimacies: List<Intimacy>,

    val essenceLevel: Int,

    val weapons: Set<EquipmentSlot<Weapon>> = setOf(),

    val armor: Set<EquipmentSlot<Armor>> = setOf(),

    val healthMods: HealthMod = HealthMod(),

    val damageTaken: DamageTaken = DamageTaken(),

    val chronicle: Chronicle // Each character carries a reference to the lists of data that they use
)

enum class Attribute {
    Strength,   Dexterity,      Stamina,
    Charisma,   Manipulation,   Appearance,
    Perception, Intelligence,   Wits;
    /*override fun toString(): String {
        return this.name.firstCap()
    }*/
}

enum class Ability {
    Archery,        Athletics,      Awareness,
    Brawl,          Bureaucracy,    Dodge,
    Integrity,      Investigation,  Larceny,
    Linguistics,    Lore,           Medicine,
    Melee,          Occult,         Performance,
    Presence,       Resistance,     Ride,
    Sail,           Socialize,      Stealth,
    Survival,       Thrown,         War;
    /*override fun toString(): String {
        return this.name.firstCap()
    }*/
}

data class MeritSlot(val meritName: String, val rating: Int, val note: String) {
    override fun toString(): String {
        return "$meritName ${getDotNotation(rating)} (note- $note)"
    }

    fun getMerit(meritList: List<Merit>) : Merit {
        meritList.forEach { if (meritName == it.name) {return it} }
        return Merit("merit not found", "Couldn't find the merit $meritName", MeritType.Flaw, listOf(1), MeritPrereqGroup(RequirementMode.Exclusive), listOf(), 1)
    }
}


data final class Intimacy(val type: IntimacyType, val intensity: IntimacyIntensity, val subject: String, val description: String)

enum class IntimacyType() {
    Principle, Tie
}

enum class IntimacyIntensity() {
    Minor, Major, Defining
}

enum class DamageType(val symbol: Char) {
    Aggravated('Ж'), Lethal('X'), Bashing('/'), None('_'), Initiative('i');
}


final data class DamageTaken(val bashing: Int = 0, val lethal: Int = 0, val aggravated: Int = 0) {
    val total : Int
        get() = bashing + lethal + aggravated
}

data class HealthMod(val zero: Int = 0, val one: Int = 0, val two: Int = 0, val four: Int = 0)



/*
-0 health levels takes one hour if bashing, or one day if lethal
-1 health levels takes 12 hours if bashing, or two days if lethal.
-2 health levels takes one day if bashing, or three days if lethal.
-4 health levels takes two days if bashing, or five days if lethal.
 */