package com.blakwurm.anathama3.character

import com.blakwurm.anathama3.getDotNotation
import com.blakwurm.anathama3.wrapAt
import javafx.scene.control.ListCell
import javafx.scene.control.cell.ComboBoxListCell

final data class Merit(override val name: String?, // Fields are all nullable because GSON can assign null values via reflection... not much I can do, so I might as well plan for it.
                  val description: String?,
                  val meritType: MeritType? = MeritType.Story,
                  val possibleRatings: List<Int>? = listOf(),
                  val prereqs: MeritPrereqGroup?,//List<MeritPrereq>? = listOf(),
                  val tags: List<String>? = listOf(),
                  var timesPerchasable: Int? = -1) : Named {
    override fun toString(): String {
        return  "$name - $meritType\n" +
                "${if(prereqs != null && !prereqs.set.isEmpty()) {"     ----- prerequisites -----\n$prereqs\n"} else {""}}" +
                "     ----- possible ratings ----- \n$possibleRatings\n" +
                "${if(tags != null && !tags.isEmpty()) {"     ----- tags ----- \n${if (tags.isEmpty()) {"No tags to speak of"} else {tags.toString()}}\n"} else {""}}" +
                "     ----- description ----- \n${description?.wrapAt(80, "")}\n" +
                "     ----- times repurchasable ----- \n ${if (timesPerchasable == -1) {"infinitely many times"} else {"${timesPerchasable} times"}}" +
                "\n\n\n"
    }
}



enum class MeritType() {
    Innate, Story, Purchased, Flaw
}

val supernaturalTag = "Supernatural"

data class MeritPrereq(val name: String, val rank: Int) {
    override fun toString(): String {
        return "$name${if (rank > 0) {" "+ getDotNotation(rank)
        } else{""}}"
    }
}

enum class RequirementMode() {Exclusive, Inclusive, ShapingRitual}

data class MeritPrereqGroup(val mode: RequirementMode, val set: Set<MeritPrereq> = setOf()) {
    infix fun using(newSet: Set<MeritPrereq>) : MeritPrereqGroup {
        return MeritPrereqGroup(mode, newSet)
    }
}

class MeritCell : ListCell<Merit>() {
    override fun updateItem(item: Merit?, empty: Boolean) {
        super.updateItem(item, empty)
        text = if (item != null) {
            "${item.name} - ${item.meritType} - Prereqs are ${item.prereqs}"
        } else {
            "Error, merit not registered."
        }
    }
}