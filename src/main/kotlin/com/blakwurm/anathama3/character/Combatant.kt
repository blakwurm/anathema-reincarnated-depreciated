package com.blakwurm.anathama3.character

/**
 * Created by Alex on 5/3/2016.
 */
data class Combatant(override val name: String,
                     val note: String,
                     val healthMod: HealthMod,
                     val damageTaken: DamageTaken,
                     val initiative: Int,
                     val personalEssence: Int,
                     val personalEssenceMax: Int,
                     val periphEssence: Int,
                     val periphEssenceMax: Int,
                     val tempWillpower: Int,
                     val willpowerMax: Int,
                     val bindWillpower: Boolean,
                     val isBattlegroup: Boolean = false,
                     val damageType: DamageType,
                     val enemiesInRange: List<String>
): Named