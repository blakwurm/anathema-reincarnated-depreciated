package com.blakwurm.anathama3.character

/**
 * Created by Alex on 4/25/2016.
 */
data class Chronicle(
        val title: String = "No Title",
        val storyteller: String = "No Storyteller",
        val description: String = "Description not set.",
        val merits: Set<Merit> = setOf(),
        val solarCharms: Set<SolarCharm> = setOf(),
        val weapons: Set<Weapon> = setOf(),
        val armor: Set<Armor> = setOf(),
        val evocation: Set<Evocation> = setOf(),
        val spell: Set<Spell> = setOf(),
        val shapingRitual: Set<ShapingRitual> = setOf()
) {
    /**
     * Combines two chronicles into a new chronicle, using the first's metadata.
     */
    infix operator fun plus(newChron:Chronicle) : Chronicle {
        return Chronicle(
                title = this.title,
                storyteller = this.storyteller,
                description = this.description,
                merits = this.merits + newChron.merits,
                solarCharms = this.solarCharms + newChron.solarCharms,
                weapons = this.weapons + newChron.weapons,
                armor = this.armor + newChron.armor,
                evocation = this.evocation + newChron.evocation,
                spell = this.spell + newChron.spell
        )
    }

    /**
     * @return A new Chronicle, containing only elements that do not exist in both the first or second chronicle. Uses
     * the first chronicle's metadata.
     */
    infix fun minus(newChron: Chronicle) : Chronicle {
        return Chronicle(
                title = this.title,
                storyteller = this.storyteller,
                description = this.description,
                merits = this.merits - newChron.merits,
                solarCharms = this.solarCharms - newChron.solarCharms,
                weapons = this.weapons - newChron.weapons,
                armor = this.armor - newChron.armor,
                evocation = this.evocation - newChron.evocation,
                spell = this.spell - newChron.spell
        )
    }
}
