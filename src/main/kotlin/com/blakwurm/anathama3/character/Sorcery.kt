package com.blakwurm.anathama3.character

/**
 * Created by Alex on 5/5/2016.
 */
data class Sorcerer(val circle: SorceryCircle = SorceryCircle.None,
                    val shapingRituals: Set<ShapingRitual> = setOf(),
                    val spells: Set<CastableSlot<Spell>> = setOf(),
                    val motes: Int = 0)

enum class SorceryCircle() {
    None, Terrestrial, Celestial, Solar
}

data class ShapingRitual(override val name: String, val description: String) : Named

