package com.blakwurm.anathama3.character

/**
 * Created by Alex on 4/22/2016.
 */

interface Equipment : Named {
    val description: String?
    val dots: Int?
    val power: Power?
    val weight: Weight?
    val cost: Int?
}

data class Weapon(override val name: String? = "No Name",
                  override val description: String? = "Error, this is a blank weapon",
                  override val dots: Int? = 0,
                  override val power: Power? = Power.Mortal,
                  override val weight: Weight? = Weight.Light,
                  val typeCategory: TypeCategory? = TypeCategory.Meele,
                  val range: Range? = Range.Close,
                  val ability: Ability? = Ability.Melee,
                  override val cost: Int? = 3,
                  val tags: List<WeaponTags>? = listOf()) : Equipment

data class Armor(override val name: String? = "No Name",
                 override val description: String? = "Error, this is a blank weapon",
                 override val dots: Int? = 0,
                 override val power: Power? = Power.Mortal,
                 override val weight: Weight? = Weight.Light,
                 override val cost: Int? = 3,
                 val tags: List<ArmorTags>? = listOf()) : Equipment


data class EquipmentSlot<T: Equipment>(override val name: String? = "Dagger",
                                       val equiped: Boolean = false,
                                       val evocations: Set<CastableSlot<Evocation>> = setOf()) : Named {
    fun getEquipmentFrom(collection: Collection<T>) : T {
        return collection.getElementWithName(this.name ?: "Dagger")
    }
}

enum class TypeCategory(val possibleTags: List<WeaponTags>) {
    Meele(meeleTags), Archery(archeryTags), Thrown(thrownTags)
}

enum class Range() {
    Close, Short, Medium, Long, Extreme
}

enum class Power() {
    Mortal, Artifact
}

enum class Weight() {
    Light, Medium, Heavy
}

enum class WeaponTags() {
    Balanced, Bashing, Brawl, Chopping, Disarming, Flexible, Improvised,
    Grappling, Lethal, MartialArts, Melee, Natural, Piercing, Reaching, Smashing,
    Thrown, TwoHanded, Worn,

    Cutting, Subtle, Poisonable,

    Archery,  Concealable, Crossbow, Flame, Mounted, OneHanded,
    Powerful, Slow, Special
}

enum class ArmorTags() {
    Bouyant, Concealable, Silent
}



val meeleTags = listOf<WeaponTags>(WeaponTags.Balanced, WeaponTags.Bashing,
        WeaponTags.Brawl, WeaponTags.Chopping, WeaponTags.Disarming,
        WeaponTags.Flexible, WeaponTags.Improvised, WeaponTags.Grappling,
        WeaponTags.Lethal, WeaponTags.MartialArts, WeaponTags.Melee,
        WeaponTags.Natural, WeaponTags.Piercing, WeaponTags.Reaching,
        WeaponTags.Smashing, WeaponTags.Thrown, WeaponTags.TwoHanded,
        WeaponTags.Worn)

val thrownTags = listOf<WeaponTags>(WeaponTags.Bashing, WeaponTags.Concealable,
        WeaponTags.Cutting, WeaponTags.Lethal, WeaponTags.Mounted,
        WeaponTags.Poisonable, WeaponTags.Special, WeaponTags.Subtle,
        WeaponTags.Thrown)

val archeryTags = listOf<WeaponTags>(WeaponTags.Archery, WeaponTags.Bashing,
        WeaponTags.Concealable, WeaponTags.Crossbow, WeaponTags.Flame,
        WeaponTags.Lethal, WeaponTags.Mounted, WeaponTags.OneHanded,
        WeaponTags.Piercing, WeaponTags.Powerful, WeaponTags.Slow, WeaponTags.Special)



