package com.blakwurm.anathama3.character

import com.blakwurm.anathama3.firstCap
import com.blakwurm.anathama3.getDotNotation
import com.blakwurm.anathama3.wrapAt
import org.apache.commons.lang3.text.WordUtils
import org.jcp.xml.dsig.internal.dom.ApacheTransform

/**
 * Created by Alex on 4/11/2016.
 */

/*
Things that are on all character sheets:
        -Anima, -Caste/Aspect,
        Weapons and Attacks,
        Armor, Derived Values, Soak,
        Resolve, Guile, -Charms.
 */
data final class Solar(val mortal: Mortal,
                       val anima: String,
                       val charms: List<CastableSlot<SolarCharm>>,
                       val caste: SolarCaste)

enum class SolarCaste {
    DAWN,ZENITH,TWILIGHT,NIGHT,ECLIPSE;
    /*override fun toString(): String {
        return this.name.firstCap()
    }*/
}






data class SolarSpecialization(val ability: Ability, val text: String) {
    override fun toString(): String {
        return "$ability ($text)"
    }
}

/*Before realizing that charm costs are more complex then what it's worth, I was going to
* have it be a case of adding costs a la merit prerequisites. Heh, no. Too much work. This
* exists just in case I (or someone else) takes the effort to really formalize costs.*/
/*enum class SolarCharmCostKind(val abbreviation: String) {
    Motes("m"), Willpower("wp"), BashingHealthLevels("hl"),
    LethalHealthLevels("lhl"), AggravatedHealthLevels("ahl"),
    AnimaLevels("a"), InitiativePoints("i"), ExperiencePoints("xp"),
    SilverCraftingXP("sxp"), GoldCraftingXP("gxp"), WhiteCraftingXP("wxp")
}*/