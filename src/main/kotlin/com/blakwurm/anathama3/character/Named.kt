package com.blakwurm.anathama3.character

/**
 * Created by Alex on 4/28/2016.
 */

interface Named {val name: String?}

fun makeNameListFrom(listOfNamedThings: Collection<Named>) : List<String> {
    val nameList: MutableList<String> = mutableListOf()
    listOfNamedThings.forEach {
        nameList.add(it.name ?: "")
    }
    return nameList.toList()
}

fun Collection<Named>.nameList() : List<String> {
    return makeNameListFrom(this)
}

fun <T: Named> Collection<T>.getElementWithName(name: String) : T {
    this.forEach { if (it.name == name) {return@getElementWithName it} }
    return this.first()
}