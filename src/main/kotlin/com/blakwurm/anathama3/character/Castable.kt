package com.blakwurm.anathama3.character

import com.blakwurm.anathama3.wrapAt

/**
 * Created by Alex on 5/5/2016.
 */

interface Castable : Named {
    val description:String?
    val type: CastableType?
    val duration:Pair<CastableDurationPeriod?, CastableDurationUnit?>?
    val useCost:String?
    val keywords:Set<CastableKeyword?>?
}

final data class SolarCharm(override val name:String = "Unnamed",
                            override val description:String = "No Description",
                            val group: String = "No Group",
                            val ability: Ability = Ability.Archery,
                            val minAbility: Int = 1,
                            val minEssence: Int = 1,
                            override val type: CastableType = CastableType.Simple,
                            override val duration:Pair<CastableDurationPeriod, CastableDurationUnit> = CastableDurationPeriod.Simply to CastableDurationUnit.Instant,
                            override val useCost:String = "1m, 5b",
                            override val keywords:Set<CastableKeyword> = setOf(),
                            val prereqCharms: String = "",
                            val repurchasable: Boolean = false) : Castable {
    override fun toString(): String {
        fun makeKeywordListPretty(list: Set<CastableKeyword>) : String {
            if (list.isEmpty()) {return "None"}
            var toReturn = ""
            CastableKeyword.values().forEach {
                toReturn += "$it, "
            }
            toReturn = toReturn.dropLast(2)
            return toReturn
        }
        fun makeDurationPretty(duration: Pair<CastableDurationPeriod, CastableDurationUnit>) : String {
            return "${if (duration.first.equals(CastableDurationPeriod.Simply)) {""} else {duration.first.toString()}} ${duration.second}"
        }
        fun makeGroupPretty(group: String) : String {
            return if (group.length > 0) {"\n     Group: $group"} else {""}
        }

        return "$name" +
                makeGroupPretty(group) +
                "\n     Cost: $useCost; Mins: $ability $minAbility, Essence $minEssence" +
                "\n     Type: $type" +
                "\n     Keywords: ${makeKeywordListPretty(keywords)}" +
                "\n     Duration: ${makeDurationPretty(duration)}" +
                "\n     Prerequisite Charms: $prereqCharms" +
                "\n\n     Description" +
                "\n     -----------" +
                "\n     ${description.wrapAt(80, "     ")} \n\n"
    }
}

data class CastableSlot<T: Castable>(val castableName: String, val note: String, val timesPurchased: Int = 0) {
    override fun toString(): String {
        return "$castableName (note- $note)"
    }
    fun getCastableFrom(charms : Collection<T>) : T {
        return charms.getElementWithName(castableName)
    }
}

data class Spell(override val name: String = "Unnamed",
                 override val description: String = "No Description",
                 override val type: CastableType = CastableType.Reflexive,
                 override val duration: Pair<CastableDurationPeriod, CastableDurationUnit> = CastableDurationPeriod.One to CastableDurationUnit.Scene,
                 override val useCost: String = "20sm, 2wp",
                 override val keywords:Set<CastableKeyword> = setOf()
                 ) : Castable


data class Evocation(override val name:String?,
                     override val description:String?,
                     val minEssence: Int?,
                     override val type: CastableType?,
                     override val duration:Pair<CastableDurationPeriod, CastableDurationUnit>?,
                     override val useCost:String?,
                     override val keywords:Set<CastableKeyword>?,
                     val prereqs: String?) : Castable

enum class CastableType() {
    Simple, Supplemental, Reflexive, Permanent
}

enum class CastableKeyword() {
    Aggravated, Bridge, Clash, Counterattack, DecisiveOnly, Dual, Mute, Pilot, Psyche,
    Perilous, Salient, Stackable, Uniform, WitheringOnly, WrittenOnly
}

enum class CastableDurationUnit() {
    Instant, Tick, Turn, Round, Hour, Dream, Scene, Sunrise, Day, Dismissal, Infinite, Permanent
}

enum class CastableDurationPeriod() {
    Simply, UntilNext, One, Essence
}

