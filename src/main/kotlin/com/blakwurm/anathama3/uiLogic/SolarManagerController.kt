package com.blakwurm.anathama3.uiLogic

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextField

/**
 * Created by Alex on 4/17/2016.
 */
class SolarManagerController {
    lateinit var app: Application
    lateinit var overview: ScrollPane
    lateinit var detailField: Label
}