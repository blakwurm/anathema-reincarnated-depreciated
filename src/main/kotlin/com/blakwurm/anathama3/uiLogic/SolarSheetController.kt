package com.blakwurm.anathama3.uiLogic

import com.github.salomonbrys.kotson.typeToken
import com.blakwurm.anathama3.LifecycleCallbacks
import com.blakwurm.anathama3.autocompleteWith
import com.blakwurm.anathama3.character.*
import com.blakwurm.anathama3.getPointValue
import com.blakwurm.anathama3.loader.loadAndlaunchWindow
import com.blakwurm.anathama3.loader.loadFileFromChooser
import com.blakwurm.anathama3.loader.loadObjectFromLocalFile
import com.blakwurm.anathama3.uiLogic.CombatTracker.HealthTrackController
import javafx.event.ActionEvent
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.util.Callback
import org.controlsfx.control.PopOver
import org.controlsfx.control.Rating
import org.controlsfx.control.textfield.TextFields
import java.net.URL
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate
import kotlin.concurrent.timer

/**
 * Controls the Solar Character Sheet. This file is organized in terms of several modules, each one intentionally
 * independent. Rather then subdivide each module into their own FXML/Controller files (like was done with the
 * sheet and the Solar Manager), development is made easier by keeping the entire sheet one in FXML/controller setup.
 *
 * If dealing with code that is only concerned with one module, methods are perfectly acceptable. However, if
 * elements from two different modules must interact, the appropriate pattern is to create a function that can be called
 * from anywhere in the codebase. If a module needs to be split off into its own section (like in the case of additional
 * sheet types) it will be a lot easier to figure out how to call a function then it will be to figure out how to
 * split apart a method. The two exceptions are when setting the detail view and when reading from (NOT writing to)
 * the attribute/ability sections.
 *
 * Yes, this is asking for intentional and mindful encapsulation without providing automatic checks against violations.
 * Someday a solution might be written up that is more hand-holdy, but the original author of this file is saving the
 * Apple-style guide-me-because-I'm-new-and-I-don't-know-how-any-of-this-works design for, y'know, making Anathema
 * easy to use for new players.
 */
class SolarSheetController : Initializable {





    /**
     * A reference to the parent SolarManager's Controller.
     */
    lateinit var manager: SolarManagerController

    /**
     * Chronicle used by the solar sheet. Setter automatically resets the lists that use it.
     */
    private var _chronicle: Chronicle = Chronicle()
    var chronicle: Chronicle
        get() = _chronicle
        set(value) {
            _chronicle = value
            meritDropdown.items.clear()
            meritDropdown.items.addAll(chronicle.merits)
            charmDropdown.items.clear()
            charmDropdown.items.addAll(chronicle.solarCharms)
            weaponNameField autocompleteWith chronicle.weapons.nameList()
            armorNameField autocompleteWith chronicle.armor.nameList()
        }

    /**
     * An abstraction method, designed to allow flexibility in how the detail view is implemented.
     *
     * Any text passed in will be shown in the detail view.
     */
    fun setDetail(toSet: String, trigger: Node) : Boolean {
        val label = Label(toSet)
        label.isWrapText = true
        val popover = PopOver(label)
        popover.arrowLocation = PopOver.ArrowLocation.TOP_CENTER
        popover.show(trigger)
        return true
    }

    lateinit var healthPane: TitledPane
    lateinit var healthTrackController: HealthTrackController

    /**
     * JavaFX-specified initialisation method. Replaces the constructor, basically. The rule about
     * modules not directly interacting *especially* applies here.
     */
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        chronicle = loadObjectFromLocalFile<Chronicle>("/bookdata/testing.chron") // loadFileFromChooser(fileHint = "Chronicle File", filetype = ".chron")
        meritDropdown.cellFactory = Callback { list -> return@Callback MeritCell() }

        casteDropdown.items.addAll(SolarCaste.values())
        casteDropdown.selectionModel.select(SolarCaste.DAWN)
        intimacyIntensityDropdown.items.addAll(IntimacyIntensity.values())
        intimacyTypeDropdown.items.addAll(IntimacyType.values())
        supernalDropdown.items.addAll(Ability.values())
        supernalDropdown.selectionModel.select(Ability.Archery)
        speicalAbilityDropdown.items.addAll(Ability.values())
        speicalAbilityDropdown.selectionModel.select(Ability.Bureaucracy)

        //weaponNameField.items.addAll(chronicle.weapons)
        //armorNameField.items.addAll(chronicle.armor)
        //derivedTimer.scheduleAtFixedRate(250,250) { calculateDerivedValues() } // sets a timer to auto-update derived stats
        //LifecycleCallbacks.stopCallbacks.add() {derivedTimer.cancel()} // when the app closes, timer stops.
        val sheetLoader = FXMLLoader()
        val track = sheetLoader.load<Node>(this.javaClass.getResource("/ui/healthtrack.fxml").openStream())
        healthPane.content = track
        healthTrackController = sheetLoader.getController()
    }

    /*
     * The character detail module.
     */

    lateinit var characterNameField: TextField
    lateinit var playerNameField: TextField
    lateinit var conceptField: TextArea
    lateinit var animaField: TextArea
    lateinit var casteDropdown: ComboBox<SolarCaste>
    lateinit var supernalDropdown: ComboBox<Ability>

    /*
     * The attribute and ability modules. Rather then provide an injection field for each
     * individual stat (which would require quite a few fields) it's much easier on everyone
     * to just pass an enum value into a method.
     */

    fun getRating(attribute: Attribute) : Rating {
        //println("Attribute is #${attribute.name}Score")
        val toReturn = characterNameField.scene.lookup("#${attribute.name}Score")
        toReturn ?: return Rating()
        return  toReturn as Rating
    }
    fun getRating(ability: Ability) : Rating {
        val toReturn = characterNameField.scene.lookup("#${ability.name}Score")
        toReturn ?: return Rating()
        return toReturn as Rating
    }
    fun getAttribute(rating: Rating) : Attribute {
        return Attribute.valueOf(rating.id.substring(0, rating.id.length - "Score".length))
    }
    fun getAbility(rating: Rating) : Ability {
        return Ability.valueOf(rating.id.substring(0, rating.id.length - "Score".length))
    }


    /*
     * Charm module. Currently barren until a charm json list is provided.
     */

    lateinit var charmDropdown: ComboBox<SolarCharm>
    lateinit var charmNoteField: TextField
    lateinit var charmListBox: ListView<CastableSlot<SolarCharm>>
    lateinit var charmDetailButton: Button

    fun onCharmSelected(actionEvent: ActionEvent) {
        setDetail(charmDropdown.selectionModel.selectedItem.toString(), charmDropdown)
    }

    fun addCharm(actionEvent: ActionEvent) {
        charmListBox.items.add(CastableSlot(castableName = charmDropdown.selectionModel.selectedItem.name, note = charmNoteField.text))
        charmDropdown.selectionModel.clearSelection()
        charmNoteField.text = ""
    }

    fun showCharmDetail(actionEvent: ActionEvent) {
        setDetail(charmListBox.selectionModel.selectedItem.getCastableFrom(chronicle.solarCharms).toString(), charmDetailButton)
    }


    /**
     * Convenience method for applicable rating widgets. All this does is make the rating 0 when double-clicked. Not
     * a part of any module and exempt from the limit on cross-module interaction
     */
    fun makeRatingZeroOnDoubleClick(event: MouseEvent) {

        if (event.clickCount == 2) {
            println("clicks registered!")
            val thing = (event.source as Rating)
            println("source is $thing, class is ${thing.javaClass}")
            thing.rating = 0.toDouble()
        }
    }

    //val derivedTimer = Timer()

    /**
     * method to update derived values. While this *is* a computation that cuts across modules, it is written as a
     * method because the method itself will never move from the main sheet.
     */
    private fun calculateDerivedValues() {
        //println("${Math.ceil(getRating(Attribute.Manipulation).rating + getRating(Ability.Socialize).rating / 2.0)}")
        characterNameField.scene ?: return
        guileField.text = "${Math.ceil(getRating(Attribute.Manipulation).rating + getRating(Ability.Socialize).rating / 2.0)}"
    }

    /*
     *
     * Specialization module
     *
     */

    lateinit var speicalAbilityDropdown: ComboBox<Ability>
    lateinit var speicalField: TextField
    lateinit var specialListView: ListView<SolarSpecialization>

    fun addSpecialization(actionEvent: ActionEvent) {
        specialListView.items.add(SolarSpecialization(ability = speicalAbilityDropdown.selectionModel.selectedItem, text = speicalField.text))
    }

    fun removeSpecialization(actionEvent: ActionEvent) {
        specialListView.items.removeAll(specialListView.selectionModel.selectedItems)
    }

    /*
     * Intimacy module
     */

    lateinit var resolveField: TextField
    lateinit var guileField: TextField


    lateinit var intimacyIntensityDropdown: ComboBox<IntimacyIntensity>
    lateinit var intimacyTypeDropdown: ComboBox<IntimacyType>
    lateinit var intimacySubjectField: TextField
    lateinit var intimacyDescField: TextField
    lateinit var intimacyListView: ListView<Intimacy>

    fun addNewIntimacy(actionEvent: ActionEvent) {
        intimacyListView.items.add(
                Intimacy(type = intimacyTypeDropdown.value,
                        intensity = intimacyIntensityDropdown.value,
                        subject = intimacySubjectField.text,
                        description = intimacyDescField.text)
        )
        intimacyDescField.text = ""
        intimacySubjectField.text = ""
    }

    fun removeSelectedIntimacy(actionEvent: ActionEvent) {
        intimacyListView.items.remove(intimacyListView.selectionModel.selectedItem)
    }


    /*
     * Willpower and limit module.
     */

    lateinit var tempWillpowerRating: Rating
    lateinit var permWillpowerRating: Rating


    lateinit var limitRating: Rating
    lateinit var limitBreakButton: Button

    fun limitClicked(event: MouseEvent) {
        makeRatingZeroOnDoubleClick(event)
        if (limitRating.rating == 10.toDouble()) { // the boolean assignment is exploded in order to facilitate changes
            limitBreakButton.isDisable = false
        } else {
            limitBreakButton.isDisable = true
        }
    }

    fun limitBreak(actionEvent: ActionEvent) {
        limitRating.rating = 0.toDouble()
        limitBreakButton.isDisable = true
        if (tempWillpowerRating.rating < permWillpowerRating.rating){
            tempWillpowerRating.rating = permWillpowerRating.rating
        }

    }

    /*
     * Merit Module
     */


    lateinit var meritDropdown: ComboBox<Merit>
    lateinit var meritRatingDropdown: ComboBox<Int>
    lateinit var meritListView: ListView<MeritSlot>
    lateinit var meritNoteField: TextField
    lateinit var meritDetailButton: Button

    fun onMeritSelected(actionEvent: ActionEvent) {
        val selectedMerit = meritDropdown.selectionModel.selectedItem
        meritRatingDropdown.items.clear()
        selectedMerit.possibleRatings?.forEach { if (it > 0) {meritRatingDropdown.items.add(it)} }
        meritRatingDropdown.selectionModel.selectFirst()
        setDetail(meritDropdown.selectionModel.selectedItem.toString(), meritDropdown)
    }

    fun addMerit(actionEvent: ActionEvent) {
        meritListView.items.add(MeritSlot(meritName = meritDropdown.selectionModel.selectedItem.name ?: "Language", rating = meritRatingDropdown.selectionModel.selectedItem ?: 1, note = meritNoteField.text))
        meritDropdown.selectionModel.clearSelection()
        meritRatingDropdown.selectionModel.selectFirst()
        meritNoteField.text = ""
    }

    fun showMeritDetail(actionEvent: ActionEvent) {
        setDetail(meritListView.selectionModel.selectedItem.getMerit(chronicle.merits.toList()).toString(), meritDetailButton)
    }


    /*
     * Weapon list module.
     */

    lateinit var weaponNameField: TextField
    lateinit var weaponBox: ListView<Weapon>

    fun addWeapon() {

        weaponBox.items.add(chronicle.weapons.getElementWithName(weaponNameField.text))
    }

    /*
     * Armor list module.
     */

    lateinit var armorNameField: TextField
    lateinit var armorBox: ListView<Armor>

    fun addArmor() {
        armorBox.items.add(chronicle.armor.getElementWithName(armorNameField.text))
    }

    /*
     * Sorcery module
     */
    lateinit var spellDropdown: ComboBox<Spell>
    lateinit var spellNoteField: TextField
    lateinit var spellListBox: ListView<CastableSlot<Spell>>
    lateinit var spellDetailButton: Button

    fun onSpellSelected() = setDetail(spellListBox.selectionModel.selectedItem.getCastableFrom(chronicle.spell.toList()).toString(), spellDropdown)
    fun showSpellDetail() = setDetail(spellListBox.selectionModel.selectedItem.getCastableFrom(chronicle.spell.toList()).toString(), spellDetailButton)
    fun setSpellAsControl() {}
    fun addSpell() {}
}