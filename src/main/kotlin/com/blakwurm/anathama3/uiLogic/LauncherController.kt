package com.blakwurm.anathama3.uiLogic

import com.blakwurm.anathama3.loader.loadAndlaunchWindow
import com.blakwurm.anathama3.uiLogic.ChronEditor.ChronEditorController
import com.blakwurm.anathama3.uiLogic.CombatTracker.CombatTrackerController
import javafx.application.Application
import javafx.event.ActionEvent
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.stage.Modality
import javafx.stage.Stage

/**
 * Anathema's starting window. Several possible branches are made available here, each one with their own controller class
 * to reference.
 */
class LauncherController {
    lateinit var app: Application
    lateinit var stage: Stage
    lateinit var solarLaunchButton: Button


    /**
     * Launches the solar tracker window. Currently the only tracker, but others will be added as more splats are
     * released by Obsidian Path.
     *
     * Loads "solarcharactermanager.fxml", and adds "solarcharsheet" to the "overview" tab. These two modules
     * are split up in order to make development easier.
     */
    fun launchSolarTracker(event: ActionEvent): Stage {
        println("solar launched!")
        val result = loadAndlaunchWindow<SolarSheetController>(application = app,
                fxmlName = "solarcharactermanager.fxml",
                cssName = "solarStyle.css",
                title = "Solar Character Manager",
                windowHeight = 750.0, windowWidth = 1200.0)
        { app, loader, subject, stageParam ->
            val managerController = loader.getController<SolarManagerController>()
            managerController.app = app
            val sheetLoader = FXMLLoader()
            val charsheet = sheetLoader.load<Node>(app.javaClass.getResource("/ui/solarcharsheet_replacement.fxml").openStream())
            managerController.overview.content = charsheet
            val sheetController = sheetLoader.getController<SolarSheetController>()
            sheetController.manager = managerController

            loader
        }
        return result.first
    }

    /**
     * Launches the merit editor. Nothing special happens here. See MeritEntryController for details on
     * functionality. This is kept separate from the character manager in order to provide a cleaner interface
     * during gameplay.
     */
/*    fun launchMeritEditer(event: ActionEvent): Stage {
        val result = loadAndlaunchWindow<MeritEntryController>(
                application = app,
                fxmlName = "meritentry.fxml",
                cssName = "meritEntryStyle.css",
                title = "Merit Entry Tool",
                windowHeight = 750.0, windowWidth = 900.0)
        { app, loader, subject, stageParam ->
            //stageParam.initModality(Modality.NONE)
            //stage.isResizable = false
            loader
        }


    }*/

    /**
     * Launches the charm editor. Nothing special happens here. See CharmEntryController for details on
     * functionality. This is kept separate from the character manager in order to provide a cleaner interface
     * during gameplay.
     */
/*    fun launchCharmEditer(event: ActionEvent): Stage {
        return loadAndlaunchWindow(
                application = app,
                fxmlName = "charmentry.fxml",
                cssName = "charmEntryStyle.css",
                title = "Solar Charm Entry Tool",
                windowHeight = 750.0, windowWidth = 1000.0)
        { app, loader, subject, stage ->

            //stage.isResizable = false

            loader
        }


    }*/

    fun launchChronEditor(event: ActionEvent): Stage {

        val result = loadAndlaunchWindow<ChronEditorController>(
                application = app,
                fxmlName = "chronEditor.fxml",
                cssName = "charmEntryStyle.css",
                title = "Chronicle Editor",
                windowHeight = 750.0, windowWidth = 1000.0)
        { app, loader, subject, stage ->

            val chronEditorController = loader.getController<ChronEditorController>()

            chronEditorController.app = app
            chronEditorController.loadListEditors()

            loader
        }
        return result.first
    }

    fun launchHealthTrackersList() {
        val result = loadAndlaunchWindow<CombatTrackerController>(application = app,
                fxmlName = "combatTracker.fxml",
                title = "Anathema Reincarnated - Combat Tracker",
                windowHeight = 600.0, windowWidth = 1050.0) {
            app, loader, subject, stage ->
            loader
        }
        result.second.app = app
    }
}