package com.blakwurm.anathama3.uiLogic.ChronEditor

import com.blakwurm.anathama3.autocompleteWith
import com.blakwurm.anathama3.character.*
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.blakwurm.anathama3.getPointValue
import com.blakwurm.anathama3.loader.loadFileFromChooser
import com.blakwurm.anathama3.loader.saveFileWithChooser
import com.blakwurm.anathama3.setPointValue
import com.blakwurm.anathama3.with
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.stage.FileChooser
import javafx.stage.Stage
import org.controlsfx.control.Rating
import org.controlsfx.control.textfield.CustomTextField
import org.controlsfx.control.textfield.TextFields
import java.net.URI
import java.util.*
import javax.annotation.Resources

/**
 * Created by Alex on 4/12/2016.
 */
class MeritEntryController : AbstractChronEditorComponent() {
    override fun applyChron(chron: ChronEditorController): (ComponentSignal) -> Boolean {
        super.applyChron(chron)
        return {
            if (it.equals(ComponentSignal.Merit)) {
                val selectedMerit = chron.meritListView.selectionModel.selectedItem ?: chron.meritListView.items.first()
                applyMeritForEditing(chron.meritListView.selectionModel.selectedItem)
                true
            } else false
        }
    }

    override val listChangedRefresher: (ComponentSignal) -> Boolean
        get() = {
            if (it.equals(ComponentSignal.Merit)) {
                refreshPrereqComboBoxes()
                true
            }
            false
        }

    lateinit var nameField: TextField
    lateinit var descriptionField: TextArea

    lateinit var typeBox: ComboBox<MeritType>

    lateinit var supernaturalCheckBox: CheckBox

    lateinit var rating1Box: CheckBox
    lateinit var rating2Box: CheckBox
    lateinit var rating3Box: CheckBox
    lateinit var rating4Box: CheckBox
    lateinit var rating5Box: CheckBox

    lateinit var repurchasable: ToggleGroup
    lateinit var notPurchasableToggle: Toggle
    lateinit var oneTimeToggle: Toggle
    lateinit var infiniteTimesToggle: Toggle
    lateinit var limitedTimesToggle: Toggle
    lateinit var limitedTimesField: TextField

    lateinit var prereqModeDropdown: ComboBox<RequirementMode>

    lateinit var prereqNameField0: TextField
    lateinit var prereqRating0: Rating
    lateinit var andor1: Label
    lateinit var prereqNameField1: TextField
    lateinit var prereqRating1: Rating
    lateinit var andor2: Label
    lateinit var prereqNameField2: TextField
    lateinit var prereqRating2: Rating
    lateinit var andor3: Label
    lateinit var prereqNameField3: TextField
    lateinit var prereqRating3: Rating
    lateinit var andor4: Label
    lateinit var prereqNameField4: TextField
    lateinit var prereqRating4: Rating
    lateinit var andor5: Label
    lateinit var prereqNameField5: TextField
    lateinit var prereqRating5: Rating


    fun initialize() { //public void initialize(Map<String, Object> namespace, URL location, Resources resources) {}
        println("Testing")
        repurchasable.selectedToggle
        typeBox.items.addAll(MeritType.values())
        typeBox.selectionModel.select(0)
        prereqModeDropdown.items.addAll(RequirementMode.values())
        prereqModeDropdown.selectionModel.selectFirst()
    }

    fun prereqModeSelected() {
        when (prereqModeDropdown.selectionModel.selectedItem) {
            RequirementMode.Exclusive -> {
                setShapingPrereqVisability(true)
                setAndOr("or")
            }
            RequirementMode.Inclusive -> {
                setShapingPrereqVisability(true)
                setAndOr("and")
            }
            RequirementMode.ShapingRitual -> {
                setShapingPrereqVisability(false)
            }
        }
    }

    fun setAndOr(text: String) {
        val andOfList = listOf(
                andor1,
                andor2,
                andor3,
                andor4,
                andor5)
        andOfList.forEach { it.text = text }
    }

    fun setShapingPrereqVisability(isVisible: Boolean) {
        val ratingList = listOf(
                prereqRating0,
                prereqRating1,
                prereqRating2,
                prereqRating3,
                prereqRating4,
                prereqRating5)

        ratingList.forEach { if (isVisible) {} else {it.rating = 0.0} }

        val nameFieldList = listOf(
                prereqNameField1,
                prereqNameField2,
                prereqNameField3,
                prereqNameField4,
                prereqNameField5)

        //nameFieldList.forEach { if (isVisible) {} else {it.text = ""} }

        val andOfList = listOf(
                andor1,
                andor2,
                andor3,
                andor4,
                andor5)

        (ratingList + nameFieldList + andOfList).forEach { it.isDisable = !isVisible }
    }

    fun addNewMerit(event: ActionEvent) {

        println("field is ${prereqNameField0.text}")

        chronEditorController.meritListView.items.add(Merit(
                name = nameField.text,
                meritType = typeBox.value,
                description = descriptionField.text,
                prereqs = MeritPrereqGroup(prereqModeDropdown.value) using setOf(
                        MeritPrereq(name = prereqNameField0.text, rank = prereqRating0.rating.toInt()),
                        MeritPrereq(name = prereqNameField1.text, rank = prereqRating1.rating.toInt()),
                        MeritPrereq(name = prereqNameField2.text, rank = prereqRating2.rating.toInt()),
                        MeritPrereq(name = prereqNameField3.text, rank = prereqRating3.rating.toInt()),
                        MeritPrereq(name = prereqNameField4.text, rank = prereqRating4.rating.toInt()),
                        MeritPrereq(name = prereqNameField5.text, rank = prereqRating5.rating.toInt())),

                possibleRatings = listOf(
                        if (rating1Box.isSelected) {
                            1
                        } else {
                            0
                        },
                        if (rating2Box.isSelected) {
                            2
                        } else {
                            0
                        },
                        if (rating3Box.isSelected) {
                            3
                        } else {
                            0
                        },
                        if (rating4Box.isSelected) {
                            4
                        } else {
                            0
                        },
                        if (rating5Box.isSelected) {
                            5
                        } else {
                            0
                        }
                ),
                tags = if (supernaturalCheckBox.isSelected) {
                    listOf(supernaturalTag)
                } else {
                    listOf()
                },
                timesPerchasable = if (repurchasable.selectedToggle.equals(oneTimeToggle)) {
                    1
                } else if (repurchasable.selectedToggle.equals(infiniteTimesToggle)) {
                    -1
                } else if (repurchasable.selectedToggle.equals(notPurchasableToggle)) {
                    0
                } else {
                    limitedTimesField.text.toInt()
                }
        ))
        try {
            clearInputBoxes()
            refreshPrereqComboBoxes()
        } catch (e:Exception) {
            println("It was the other methods!")
            e.printStackTrace()
        }

    }



    fun removeSelectedMerit() {
        chronEditorController.meritListView.items.removeAll(chronEditorController.meritListView.selectionModel.selectedItems)
        refreshPrereqComboBoxes()
    }



    fun applyMeritForEditing(selectedMerit: Merit) {


        clearInputBoxes()

        nameField.text = selectedMerit.name
        descriptionField.text = selectedMerit.description
        if (selectedMerit.possibleRatings != null) {
            val possList = selectedMerit.possibleRatings
            if (possList.contains(1)) {
                rating1Box.isSelected = true
            }
            if (possList.contains(2)) {
                rating2Box.isSelected = true
            }
            if (possList.contains(3)) {
                rating3Box.isSelected = true
            }
            if (possList.contains(4)) {
                rating4Box.isSelected = true
            }
            if (possList.contains(5)) {
                rating5Box.isSelected = true
            }
        }

        /*
         * Set the supernatural tag
         */
        if (selectedMerit.tags != null) {
            if (selectedMerit.tags.contains(supernaturalTag)) {
                supernaturalCheckBox.isSelected = true
            }
        }

        /*
         * Sets the prerequisites
         */
        if (selectedMerit.prereqs != null) {
            /**
             * Function that sets the value of a given [ComboBox] and [Rating] given a [MeritPrereq]. Written with a single
             * [Triple] as input so that it can be passed into forEach() without any hassle
             */
            fun addMerits(triple: Triple<MeritPrereq, TextField, Rating>) {
                if (triple.first.name.isNotBlank()) { // Handle blank requirements
                    triple.second.text = (triple.first as MeritPrereq).name // set merit prereq name
                    triple.third.rating = (triple.first as MeritPrereq).rank.toDouble() // set merit prereq rating
                }
            }

            // rather then screw around with reflection, the list is just hardcoded. Known requirements means we can optimize for initial developer time
            val defaultPrereq = MeritPrereq("", 0)
            val listOfPrereqs = selectedMerit.prereqs.set.toList()
            listOf(
                    listOfPrereqs.getOrElse(0) {defaultPrereq} to prereqNameField0 with prereqRating0,
                    listOfPrereqs.getOrElse(1) {defaultPrereq} to prereqNameField1 with prereqRating1,
                    listOfPrereqs.getOrElse(2) {defaultPrereq} to prereqNameField2 with prereqRating2,
                    listOfPrereqs.getOrElse(3) {defaultPrereq} to prereqNameField3 with prereqRating3,
                    listOfPrereqs.getOrElse(4) {defaultPrereq} to prereqNameField4 with prereqRating4,
                    listOfPrereqs.getOrElse(5) {defaultPrereq} to prereqNameField5 with prereqRating5
            ).forEach(::addMerits)
        }


        typeBox.value = selectedMerit.meritType
        if (typeBox.value == null) {
            typeBox.value = MeritType.Story
        }

        if (selectedMerit.timesPerchasable == -1) {
            infiniteTimesToggle.isSelected = true
        } else if (selectedMerit.timesPerchasable == 1) {
            oneTimeToggle.isSelected = true
        } else {
            limitedTimesToggle.isSelected = true
            limitedTimesField.text = selectedMerit.timesPerchasable.toString()
        }
        removeSelectedMerit()
    }

    fun clearInputBoxes() {
        nameField.text = ""
        descriptionField.text = ""

        typeBox.selectionModel.select(0)

        rating1Box.isSelected = false
        rating2Box.isSelected = false
        rating3Box.isSelected = false
        rating4Box.isSelected = false
        rating5Box.isSelected = false

        supernaturalCheckBox.isSelected = false

        oneTimeToggle.isSelected = false
        infiniteTimesToggle.isSelected = true
        limitedTimesToggle.isSelected = false
        limitedTimesField.text = ""


        prereqNameField0.text = ""
        prereqNameField1.text = ""
        prereqNameField2.text = ""
        prereqNameField3.text = ""
        prereqNameField4.text = ""
        prereqNameField5.text = ""
        prereqRating0.rating = 1.0
        prereqRating1.rating = 1.0
        prereqRating2.rating = 1.0
        prereqRating3.rating = 1.0
        prereqRating4.rating = 1.0
        prereqRating5.rating = 1.0

    }

    fun refreshPrereqComboBoxes() {
        fun addPrereqNamesToComboBox(field: TextField) {
           field autocompleteWith chronEditorController.meritListView.items.nameList()
        }
        listOf(prereqNameField0,
                prereqNameField1,
                prereqNameField2,
                prereqNameField3,
                prereqNameField4,
                prereqNameField5
                ).forEach(::addPrereqNamesToComboBox)
    }
}