package com.blakwurm.anathama3.uiLogic.ChronEditor

/**
 * Created by Alex on 4/25/2016.
 */
interface ChronEditorComponent {
    /**
     * This method is called by the controlling [ChronEditorController], and the output is put into a member list.
     *
     * @param chron The [ChronEditorController] that this component is a part of
     * @return A listener. A list of these listeners are called in sequence, and a [ComponentSignal] is passed in. If
     * the component accepts the signal, it sets the currently-selected element in its preferred list to be edited
     * in its ui, then it returns true if handled. If unhandled, it returns false.
     */
    fun applyChron(chron: ChronEditorController) : (ComponentSignal) -> Boolean

    val listChangedRefresher: (ComponentSignal) -> Boolean

    /**
     * The component's reference to their containing [ChronEditorController].
     */
    val chronEditorController: ChronEditorController
}


enum class ComponentSignal() {
    SolarCharm, Merit, Weapon, Armor, Evocation, Spell, ShapingRitual
}

abstract class AbstractChronEditorComponent : ChronEditorComponent {
    override fun applyChron(chron: ChronEditorController) : (ComponentSignal) -> Boolean {
        chronEditorController = chron
        return {true}
    }
    override val listChangedRefresher : (ComponentSignal) -> Boolean = {false}
    override lateinit var chronEditorController: ChronEditorController
}