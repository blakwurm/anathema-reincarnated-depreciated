package com.blakwurm.anathama3.uiLogic.ChronEditor

import com.blakwurm.anathama3.character.*
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.scene.control.ComboBox
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import org.controlsfx.control.ListSelectionView
import org.controlsfx.control.Rating

/**
 * Created by Alex on 4/22/2016.
 */
class WeaponEditorController : AbstractChronEditorComponent() {
    override fun applyChron(chron: ChronEditorController): (ComponentSignal) -> Boolean {
        super.applyChron(chron)
        return {
            if (it.equals(ComponentSignal.Weapon)) {
                applyWeaponForEditing(chron.weaponsListView.selectionModel.selectedItem)
                true
            } else false
        }
    }
    //lateinit var weaponList: ObservableList<Weapon>

    lateinit var tagLists: ListSelectionView<WeaponTags>

    lateinit var nameField: TextField
    lateinit var powerDropdown: ComboBox<Power>
    lateinit var weightDropdown: ComboBox<Weight>
    lateinit var rangeDropdown: ComboBox<Range>
    lateinit var weaponRating: Rating
    lateinit var costRating: Rating
    lateinit var categoryDropdown: ComboBox<TypeCategory>
    lateinit var abilityDropdown: ComboBox<Ability>
    lateinit var descriptionArea: TextArea

    fun initialize() {
        //tagLists.sourceItems.addAll(meeleTags)
        powerDropdown.items.addAll(Power.values())
        weightDropdown.items.addAll(Weight.values())
        categoryDropdown.items.addAll(TypeCategory.values())
        rangeDropdown.items.addAll(Range.values())
        abilityDropdown.items.addAll(Ability.values())
        resetInputs()
    }

    fun addWeapon(actionEvent: ActionEvent) {
        chronEditorController.weaponsListView.items.add(
                Weapon(name = nameField.text,
                        description = descriptionArea.text,
                        power = powerDropdown.value,
                        cost = costRating.rating.toInt(),
                        typeCategory = categoryDropdown.value,
                        weight = weightDropdown.value,
                        dots = weaponRating.rating.toInt(),
                        tags = tagLists.targetItems.toList(),
                        ability = abilityDropdown.value,
                        range = rangeDropdown.value)
        )
        resetInputs()
    }

    fun resetInputs() {
        tagLists.targetItems.clear()
        tagLists.sourceItems.clear()

        nameField.text = ""
        powerDropdown.selectionModel.select(Power.Mortal)
        powerSelected()
        weightDropdown.selectionModel.select(Weight.Medium)
        costRating.rating = 1.toDouble()
        categoryDropdown.selectionModel.select(TypeCategory.Meele)
        categorySelected()
        abilityDropdown.selectionModel.select(Ability.Melee)
        descriptionArea.text = ""
    }

    fun powerSelected() {
        if (powerDropdown.value.equals(Power.Artifact)) {
            weaponRating.rating = 3.toDouble()
        } else {
            weaponRating.rating = 0.toDouble()
        }
    }

    fun ratingClicked() {
        val origRating = weaponRating.rating
        val power = powerDropdown.selectionModel.selectedItem ?: Power.Mortal

        weaponRating.rating = if (power.equals(Power.Artifact)) {
            if (origRating < 3) {
                3.toDouble()
            } else {
                origRating
            }
        } else {
            0.toDouble()
        }
    }

    fun categorySelected() {
        rangeDropdown.items.clear()
        if (categoryDropdown.value.equals(TypeCategory.Meele)) {
            rangeDropdown.items.add(Range.Close)
            rangeDropdown.selectionModel.selectFirst()
        } else {
            rangeDropdown.items.addAll(Range.Short, Range.Medium,
                    Range.Long, Range.Extreme)
            rangeDropdown.selectionModel.selectFirst()
        }
        //tagLists.targetItems.clear() We intentionally do not clear the applied tags
        tagLists.sourceItems.clear()
        tagLists.sourceItems.addAll(categoryDropdown.value.possibleTags)
        tagLists.sourceItems.removeAll(tagLists.targetItems)
    }

    fun applyWeaponForEditing(weapon: Weapon) : Boolean {
        nameField.text = weapon.name
        powerDropdown.selectionModel.select(weapon.power)
        weightDropdown.selectionModel.select(weapon.weight)
        categoryDropdown.selectionModel.select(weapon.typeCategory)
        rangeDropdown.selectionModel.select(weapon.range)
        weaponRating.rating = (weapon.dots ?: 0).toDouble()
        tagLists.targetItems.clear()
        tagLists.targetItems.addAll(weapon.tags ?: listOf())

        return true
    }
}