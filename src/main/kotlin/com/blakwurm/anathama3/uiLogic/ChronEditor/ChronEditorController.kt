package com.blakwurm.anathama3.uiLogic.ChronEditor

import com.blakwurm.anathama3.character.*
import com.blakwurm.anathama3.loader.loadFileFromChooser
import com.blakwurm.anathama3.loader.saveFileWithChooser
import com.blakwurm.anathama3.removeSelected
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.control.ListView
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import java.net.URL
import java.util.*
import kotlin.concurrent.thread

/**
 * Created by Alex on 4/24/2016.
 */
class ChronEditorController : Initializable {

    lateinit var app: Application

    lateinit var titleField: TextField
    lateinit var storytellerField: TextField
    lateinit var descriptionField: TextArea

    lateinit var meritsContent: ScrollPane
    lateinit var meritListView: ListView<Merit>

    lateinit var solarCharmContent: ScrollPane
    lateinit var solarCharmListView: ListView<SolarCharm>

    lateinit var weaponsContent: ScrollPane
    lateinit var weaponsListView: ListView<Weapon>

    lateinit var armorContent: ScrollPane
    lateinit var armorListView: ListView<Armor>

    lateinit var evocationContent: ScrollPane
    lateinit var evocationListView: ListView<Evocation>

    lateinit var spellContent: ScrollPane
    lateinit var spellListView: ListView<Spell>

    lateinit var shapingRitualContent: ScrollPane
    lateinit var shapingRitualListView: ListView<ShapingRitual>

    private val editList: MutableList<(ComponentSignal) -> Boolean> = mutableListOf()
    private val changedList: MutableList<(ComponentSignal) -> Boolean> = mutableListOf()

    override fun initialize(location: URL?, resources: ResourceBundle?) {
    }

    fun loadListEditors() {

        fun processCronElement(toApply: Pair<String, ScrollPane>) : Pair<(ComponentSignal) -> Boolean, (ComponentSignal) -> Boolean>{
            var sheetLoader = FXMLLoader()
            val chronElement = sheetLoader.load<Node>(app.javaClass.getResource(toApply.first).openStream())
            toApply.second.content = chronElement
            val controller = sheetLoader.getController<ChronEditorComponent>()
            return controller.applyChron(this) to controller.listChangedRefresher
        }

        val directory = "/ui/chronentries/"
        listOf( "${directory}meritentry.fxml" to meritsContent,
                "${directory}charmentry.fxml" to solarCharmContent,
                "${directory}weaponbuilder.fxml" to weaponsContent,
                "${directory}armorbuilder.fxml" to armorContent,
                "${directory}evocationentry.fxml" to evocationContent,
                "${directory}spellentry.fxml" to spellContent,
                "${directory}shapingritualentry.fxml" to shapingRitualContent
                ).forEach() {
                    val functions = processCronElement(it)
                    editList.add(functions.first)
                    changedList.add(functions.second)
        }
    }

    fun loadChronFromFile() {
        clearLists()
        setChronToEdit(getExternalChronFile())
    }

    /**
     * The use of [Set] as the [Chronicle]'s collection implementation means that it automatically
     * checks for duplication. Much easier (and trivially expensive) to re-fresh the lists using Chronicle's
     * own add method.
     */
    fun mergeChronFromFile() {
        clearLists()
        setChronToEdit(this.getChronicle() + getExternalChronFile())
    }

    private fun getExternalChronFile() : Chronicle {
        return loadFileFromChooser<Chronicle>(fileHint = "Chronicle File", filetype = ".chron")
    }

    fun saveChronFile() {
        saveFileWithChooser(getChronicle(), "Chronicle File", ".chron")
    }

    fun getChronicle() : Chronicle {
        return Chronicle(
                title = titleField.text,
                storyteller = storytellerField.text,
                description = descriptionField.text,
                solarCharms = solarCharmListView.items.toSet(),
                merits = meritListView.items.toSet(),
                weapons = weaponsListView.items.toSet(),
                armor = armorListView.items.toSet(),
                evocation = evocationListView.items.toSet(),
                spell = spellListView.items.toSet(),
                shapingRitual = shapingRitualListView.items.toSet()
        )
    }

    private fun clearLists() {
        meritListView.items.clear()
        solarCharmListView.items.clear()
        weaponsListView.items.clear()
        armorListView.items.clear()
        evocationListView.items.clear()
        spellListView.items.clear()
        shapingRitualListView.items.clear()
    }

    fun setChronToEdit(chron: Chronicle) {
        titleField.text = chron.title
        descriptionField.text = chron.description
        storytellerField.text = chron.storyteller
        meritListView.items.addAll(chron.merits)
        solarCharmListView.items.addAll(chron.solarCharms)
        weaponsListView.items.addAll(chron.weapons)
        armorListView.items.addAll(chron.armor)
        evocationListView.items.addAll(chron.evocation)
        spellListView.items.addAll(chron.spell)
        shapingRitualListView.items.addAll(chron.shapingRitual)
        signalAllChanged()
    }

    private fun signalEditors(signal: ComponentSignal) : Boolean {
        editList.forEach { if (it(signal)) return true }
        return false
    }

    private fun signalChanged(signal: ComponentSignal) : Boolean {
        changedList.forEach { if (it(signal)) return true }
        return false
    }

    private fun signalAllChanged() = ComponentSignal.values().forEach { signalChanged(it) }

    fun editThing(signal: ComponentSignal, remove: () -> Unit) {
        signalEditors(signal)
        remove()
    }
    fun editMerit() = editThing(ComponentSignal.Merit) {removeMerit()}
    fun editArmor() = editThing(ComponentSignal.Armor) {removeArmor()}
    fun editWeapon() = editThing(ComponentSignal.Weapon) {removeWeapon()}
    fun editSolarCharm() = editThing(ComponentSignal.SolarCharm) {removeSolarCharm()}
    fun editEvocation()  = editThing(ComponentSignal.Evocation) {removeEvocation()}
    fun editSpell()  = editThing(ComponentSignal.Spell) {removeSpell()}
    fun editShapingRitual() = editThing(ComponentSignal.ShapingRitual) {removeShapingRitual()}

    fun doubleClick(mouseEvent: MouseEvent, edit: () -> Unit) = if (mouseEvent.clickCount == 2) { edit() } else {}

    fun doubleClickMerit(mouseEvent: MouseEvent) = doubleClick(mouseEvent) {editMerit()}
    fun doubleClickArmor(mouseEvent: MouseEvent) = doubleClick(mouseEvent) {editArmor()}
    fun doubleClickWeapon(mouseEvent: MouseEvent) = doubleClick(mouseEvent) {editWeapon()}
    fun doubleClickSolarCharm(mouseEvent: MouseEvent) = doubleClick(mouseEvent) {editSolarCharm()}
    fun doubleClickEvocation(mouseEvent: MouseEvent)  = doubleClick(mouseEvent) {editEvocation()}
    fun doubleClickSpell(mouseEvent: MouseEvent)  = doubleClick(mouseEvent) {editSpell()}
    fun doubleClickShapingRitual(mouseEvent: MouseEvent) = doubleClick(mouseEvent) {editShapingRitual()}


    fun <T> removeThing(listView: ListView<T>, signal: ComponentSignal){
        listView.removeSelected()
        signalChanged(signal)
    }
    fun removeMerit() = removeThing(meritListView, ComponentSignal.Merit)
    fun removeSolarCharm() = removeThing(solarCharmListView, ComponentSignal.SolarCharm)
    fun removeWeapon() = removeThing(weaponsListView, ComponentSignal.Weapon)
    fun removeArmor() = removeThing(armorListView, ComponentSignal.Armor)
    fun removeEvocation() = removeThing(evocationListView, ComponentSignal.Evocation)
    fun removeSpell() = removeThing(spellListView, ComponentSignal.Spell)
    fun removeShapingRitual() = removeThing(shapingRitualListView, ComponentSignal.ShapingRitual)

}