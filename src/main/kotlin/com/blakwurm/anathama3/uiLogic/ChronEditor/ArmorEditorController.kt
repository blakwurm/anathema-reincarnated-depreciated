package com.blakwurm.anathama3.uiLogic.ChronEditor

import com.blakwurm.anathama3.character.*
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.scene.control.ComboBox
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import org.controlsfx.control.ListSelectionView
import org.controlsfx.control.Rating

/**
 * Created by Alex on 4/22/2016.
 */
class ArmorEditorController : AbstractChronEditorComponent() {
    override fun applyChron(chron: ChronEditorController): (ComponentSignal) -> Boolean {
        super.applyChron(chron)
        return {
            if (it.equals(ComponentSignal.Armor)) {
                applyArmorForEditing(chron.armorListView.selectionModel.selectedItem)
                true
            } else false

        }
    }
    //lateinit var weaponList: ObservableList<Weapon>

    lateinit var tagLists: ListSelectionView<ArmorTags>

    lateinit var nameField: TextField
    lateinit var powerDropdown: ComboBox<Power>
    lateinit var weightDropdown: ComboBox<Weight>
    lateinit var weaponRating: Rating
    lateinit var costRating: Rating
    lateinit var descriptionArea: TextArea

    //lateinit override var chronEditorController: ChronEditorController

    fun initialize() {
        //tagLists.sourceItems.addAll(meeleTags)
        powerDropdown.items.addAll(Power.values())
        weightDropdown.items.addAll(Weight.values())
        resetInputs()
    }

    fun addWeapon(actionEvent: ActionEvent) {
        chronEditorController.armorListView.items.add(
                Armor(name = nameField.text,
                        description = descriptionArea.text,
                        power = powerDropdown.value,
                        cost = costRating.rating.toInt(),
                        weight = weightDropdown.value,
                        dots = weaponRating.rating.toInt(),
                        tags = tagLists.targetItems.toList())
        )
        resetInputs()
    }

    fun resetInputs() {
        tagLists.targetItems.clear()
        tagLists.sourceItems.clear()
        tagLists.sourceItems.addAll(ArmorTags.values())

        nameField.text = ""
        powerDropdown.selectionModel.select(Power.Mortal)
        powerSelected()
        weightDropdown.selectionModel.select(Weight.Medium)
        costRating.rating = 1.toDouble()
        descriptionArea.text = ""
    }

    fun powerSelected() {
        if (powerDropdown.value.equals(Power.Artifact)) {
            weaponRating.rating = 3.toDouble()
        } else {
            weaponRating.rating = 0.toDouble()
        }
    }

    fun ratingClicked() {
        val origRating = weaponRating.rating
        val power = powerDropdown.selectionModel.selectedItem ?: Power.Mortal

        weaponRating.rating = if (power.equals(Power.Artifact)) {
            if (origRating < 3) {
                3.toDouble()
            } else {
                origRating
            }
        } else {
            0.toDouble()
        }
    }

    /*fun categorySelected(actionEvent: ActionEvent) {
        rangeDropdown.items.clear()
        if (categoryDropdown.value.equals(TypeCategory.Meele)) {
            rangeDropdown.items.add(Range.Close)
            rangeDropdown.selectionModel.selectFirst()
        } else {
            rangeDropdown.items.addAll(Range.Short, Range.Medium,
                    Range.Long, Range.Extreme)
            rangeDropdown.selectionModel.selectFirst()
        }
        //tagLists.targetItems.clear() We intentionally do not clear the applied tags
        tagLists.sourceItems.clear()
        //tagLists.sourceItems.addAll(categoryDropdown.value.possibleTags)
        tagLists.sourceItems.removeAll(tagLists.targetItems)
    }*/

    fun applyArmorForEditing(armor: Armor) : Boolean {
        tagLists.targetItems.clear()
        tagLists.targetItems.addAll(armor.tags ?: listOf())
        nameField.text = armor.name
        powerDropdown.selectionModel.select(armor.power)
        powerSelected()
        weightDropdown.selectionModel.select(armor.weight)
        descriptionArea.text = armor.description
        weaponRating.rating = (armor.dots ?: 0).toDouble()
        ratingClicked()
        //tagLists.targetItems.clear()

        return true
    }
}