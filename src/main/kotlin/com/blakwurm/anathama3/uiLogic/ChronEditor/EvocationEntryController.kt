package com.blakwurm.anathama3.uiLogic.ChronEditor

import com.blakwurm.anathama3.character.*
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.blakwurm.anathama3.getPointValue
import com.blakwurm.anathama3.loader.loadFileFromChooser
import com.blakwurm.anathama3.loader.saveFileWithChooser
import com.blakwurm.anathama3.setPointValue
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.scene.layout.TilePane
import javafx.stage.FileChooser
import javafx.stage.Stage
import org.controlsfx.control.ListSelectionView
import org.controlsfx.control.Rating
import java.net.URI
import java.util.*
import javax.annotation.Resources

/**
 * Created by Alex on 4/12/2016.
 */
class EvocationEntryController : AbstractChronEditorComponent() {
    override fun applyChron(chron: ChronEditorController): (ComponentSignal) -> Boolean {
        super.applyChron(chron)
        return {
            if (it.equals(ComponentSignal.Evocation)) {
                applyEvocationForEditing(chron.evocationListView.selectionModel.selectedItem)
                true
            } else false
        }
    }




    lateinit var minEssence: Rating
    lateinit var nameField: TextField
    lateinit var useCostField: TextField
    lateinit var prereqField: TextField
    lateinit var descriptionField: TextArea
    lateinit var typeBox: ComboBox<CastableType>
    lateinit var durationPeriodBox: ComboBox<CastableDurationPeriod>
    lateinit var durationUnitBox: ComboBox<CastableDurationUnit>
    lateinit var keywordListView: ListSelectionView<CastableKeyword>


    //lateinit override var chronEditorController: ChronEditorController

    fun initialize() { //public void initialize(Map<String, Object> namespace, URL location, Resources resources) {}
        typeBox.items.addAll(CastableType.values())
        durationPeriodBox.items.addAll(CastableDurationPeriod.values())
        durationUnitBox.items.addAll(CastableDurationUnit.values())
        println("Testing")

        typeBox.selectionModel.select(CastableType.Simple)
        durationPeriodBox.selectionModel.select(CastableDurationPeriod.Simply)
        durationUnitBox.selectionModel.select(CastableDurationUnit.Instant)
        /*repurchasable.selectedToggle
        typeBox.items.addAll(MeritType.values())
        typeBox.selectionModel.select(0)*/
        clearInputBoxes()
        //println(keywords.children)
    }

    /*fun doubleClickSetToEdit(event: MouseEvent) {
        if (event.clickCount == 2) {
            println("clicks registered!")
            setCharmForEditing(ActionEvent())
        }
    }

    fun save(event: ActionEvent) {
        saveFileWithChooser(charmListView.items, "Charm List", ".charmlist")
    }

    fun load(event: ActionEvent) {
        charmListView.items.addAll(loadFileFromChooser<ArrayList<SolarCharm>>("Charm List", ".charmlist"))
    }

    fun clearCharms(event: ActionEvent) {
        charmListView.items.clear()
    }

    fun makeCharmFileChoosers() : List<FileChooser.ExtensionFilter> {
        return listOf(
                FileChooser.ExtensionFilter("Charm List", "*.charmlist"),
                FileChooser.ExtensionFilter("all", "*.*")
        )
    }*/


    fun addNewEvocation(event: ActionEvent) {
        chronEditorController.evocationListView.items.add(
                Evocation(
                        name = nameField.text,
                        description = descriptionField.text,
                        minEssence = minEssence.rating.toInt(),
                        type = typeBox.value,
                        duration = Pair(durationPeriodBox.value, durationUnitBox.value),
                        useCost = useCostField.text,
                        keywords = keywordListView.targetItems.toSet(),
                        prereqs = prereqField.text

                )
        )
        clearInputBoxes()
    }







    fun applyEvocationForEditing(evocation: Evocation) {
        clearInputBoxes()
        nameField.text = evocation.name
        descriptionField.text = evocation.description
        minEssence.rating = evocation.minEssence?.toDouble() ?: 1.0
        typeBox.selectionModel.select(evocation.type)
        durationUnitBox.selectionModel.select(evocation.duration?.second)
        durationPeriodBox.selectionModel.select(evocation.duration?.first)
        useCostField.text = evocation.useCost
        keywordListView.targetItems.addAll(evocation.keywords?.asIterable() ?: listOf())
        keywordListView.sourceItems.removeAll(evocation.keywords?.asIterable() ?: listOf())
    }

    fun clearInputBoxes() {
        nameField.text = ""
        descriptionField.text = ""
        //abilityBox.selectionModel.select(Ability.Bureaucracy) // If we're entering a list of charms, they are likely in the same ability group
        minEssence.rating = 1.0
        typeBox.selectionModel.select(CastableType.Simple)
        durationUnitBox.selectionModel.select(CastableDurationUnit.Instant)
        useCostField.text = ""
        keywordListView.targetItems.clear()
        keywordListView.sourceItems.clear()
        keywordListView.sourceItems.addAll(CastableKeyword.values())
        prereqField.text = ""
        //categoryBox.selectionModel.select(SolarCharmCategory.Normal) // Same for ability, entering charms back-to-back means that they're probably in the same category
        //groupField.text = "" // likewise for group. They're not always going to have one, but if they do then subsequent charms will probably be the same way



    }

    tailrec fun makeMapOfKeywords(listOfCheckboxes: ObservableList<Node>, map: Map<CastableKeyword, Boolean> = mapOf(), i: Int = 0) : Map<CastableKeyword, Boolean>{
        if (listOfCheckboxes.size <= i) {
            return map
        }

        if (listOfCheckboxes.get(i) is CheckBox) {
            val checkbox = listOfCheckboxes.get(i) as CheckBox
              if (checkbox.isSelected) {
                return makeMapOfKeywords(listOfCheckboxes, map.plus(Pair(CastableKeyword.valueOf(checkbox.id), checkbox.isSelected)), i+1)
            }
        }
            return makeMapOfKeywords(listOfCheckboxes, map, i+1) // if either the node isn't a checkbox *or* the checkbox isn't checked, we just move to the next thing
    }

    fun unpackMapOfKeywords(checkBoxList: ObservableList<Node>, map: Map<CastableKeyword, Boolean>) : ObservableList<Node> {
        for (key in map.keys) {
            if (map[key] == true) {
                checkBoxList.forEach {
                    if (it is CheckBox && it.id == key.name) {
                        it.isSelected = true
                    }
                }
            }
        }
        return checkBoxList
    }
}