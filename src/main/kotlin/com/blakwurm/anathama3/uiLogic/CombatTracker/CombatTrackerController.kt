package com.blakwurm.anathama3.uiLogic.CombatTracker

import com.blakwurm.anathama3.*
import com.blakwurm.anathama3.character.*
import com.blakwurm.anathama3.loader.loadFileFromChooser
import com.blakwurm.anathama3.loader.saveFileWithChooser
import com.blakwurm.anathama3.loader.showDialog
import com.blakwurm.anathama3.uiLogic.CombatTracker.HealthTrackController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Stage
import org.controlsfx.control.Notifications
import org.controlsfx.control.Rating
import org.controlsfx.control.textfield.TextFields
import java.io.File
import java.net.URL
import java.util.*
import kotlin.comparisons.compareBy

/**
 * Created by Alex on 4/29/2016.
 */
class CombatTrackerController : Initializable {
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        addCombatSlot()
    }
    var attackConfirmed = false
    lateinit var attackerBox: ComboBox<String>
    lateinit var defenderBox: ComboBox<String>
    lateinit var damageAmountField: TextField
    lateinit var witheringButton: Button
    lateinit var decisiveButton: Button
    val inRangeString = "Everything In Range"


    fun makeWitheringAttack() = makeAttack(generateAttackResult(AttackMode.Withering))
    fun makeDecisiveAttack() = makeAttack(generateAttackResult(AttackMode.Decisive))
    fun generateAttackResult(mode: AttackMode) : AttackResult = try{
        AttackResult(attackConfirmed, attackerBox.selectionModel.selectedItem, defenderBox.selectionModel.selectedItem, damageAmountField.text.tryAsInt().result, mode)
    }
    catch (e: Exception) {
        AttackResult(false, "Error!", e.message ?: "Something went wrong", 0, AttackMode.Withering)
    }


    fun refreshNameMenus(listOfNames: List<String> = this.slots.keys.nameList(), refreshAttackers:Boolean = true, refreshDefenders: Boolean = true)  {
        if (refreshAttackers) {
            attackerBox.items.clear()
            attackerBox.items.addAll(listOfNames)
            attackerBox.selectionModel.selectFirst()
        }
        if (refreshDefenders) {
            defenderBox.items.clear()
            defenderBox.items.addAll(listOfNames)
            defenderBox.selectionModel.selectLast()
        }
        this.slots.keys.forEach { it.refreshEnemiesInRangeSelection() }
    }

    fun attackerSelected() {
        try {
            val attacker = slots.keys.getElementWithName(attackerBox.selectionModel.selectedItem) as CombatTrackerSlotController
            if (attacker.isBattlegroupCheck.isSelected) {
                defenderBox.items.clear()
                defenderBox.items.add(inRangeString)
                defenderBox.selectionModel.selectFirst()
                decisiveButton.isDisable = true
            } else {
                refreshNameMenus(refreshAttackers = false)
                defenderBox.items.remove(attacker.name)
                decisiveButton.isDisable = false
            }
        } catch (e: IllegalStateException) {
            // We don't care about these right now.
        }

    }

    fun decDamage() = incTextFieldNumber(-1, 99999, damageAmountField)
    fun incDamage() = incTextFieldNumber(1, 99999, damageAmountField)

    fun addCombatSlot() : Pair<CombatTrackerSlotController, Node> {
        val sheetLoader = FXMLLoader()
        val slot = sheetLoader.load<Node>(this.javaClass.getResource("/ui/combattrackerview.fxml").openStream())
        val slotContainer = VBox()
        slotContainer.isFillWidth = true
        slotContainer.children.addAll(
                //Separator(Orientation.HORIZONTAL),
                //TextFields.createClearableTextField(),
                slot
                //Separator(Orientation.HORIZONTAL)
        )
        slotsVBox.children.addAll(slotContainer)
        val result = sheetLoader.getController<CombatTrackerSlotController>() to slotContainer
        slots.put(result.first, result.second)
        result.first.trackerController = this
        return result
    }

    fun loadCombatant() {
        val combatant : Combatant = loadFileFromChooser(fileHint = "Combatant File", filetype = ".combatant")
        val slot = addCombatSlot()
        val slotController = slot.first
        setCombatantToSlot(combatant, slotController)

    }

    fun setCombatantToSlot(combatant: Combatant, slotController: CombatTrackerSlotController) : CombatTrackerSlotController {
        slotController.nameField.text = combatant.name
        slotController.notesArea.text = combatant.note
        slotController.healthTrackController.currentDamage = combatant.damageTaken
        slotController.healthTrackController.currentHealthMod = combatant.healthMod
        slotController.healthTrackController.refreshTrack()
        slotController.initField.text = "${combatant.initiative}"
        slotController.periphEssenceField.text = "${combatant.periphEssence}"
        slotController.maxPeriphField.text = "${combatant.periphEssenceMax}"
        slotController.personalEssenceField.text = "${combatant.personalEssence}"
        slotController.maxPersonalField.text = "${combatant.personalEssenceMax}"
        slotController.tempWillpowerRating.rating = combatant.tempWillpower.toDouble()
        slotController.maxWillpowerRating.rating = combatant.willpowerMax.toDouble()
        slotController.willpowerBindCheck.isSelected = combatant.bindWillpower
        slotController.isBattlegroupCheck.isSelected = combatant.isBattlegroup
        slotController.enemiesInRangeListSelector.targetItems.addAll(combatant.enemiesInRange)
        slotController.setDamageType(combatant.damageType)
        return slotController
    }

    fun sortChildrenByInitiative() {
        val sortedControllers = slots.keys.sorted()
        slotsVBox.children.clear()
        sortedControllers.forEach { slotsVBox.children.add(slots.get(it)) }
    }

    fun removeSlot(slotController: CombatTrackerSlotController) {
        slots.remove(slotController)
        sortChildrenByInitiative()
        refreshNameMenus()
    }

    fun makeAttack(atk: AttackResult) {

        val attacker = slots.keys.getElementWithName(atk.nameOfAttacker) as CombatTrackerSlotController
        val defender = slots.keys.getElementWithName(atk.nameOfDefender) as CombatTrackerSlotController

        fun makeAttackOnDefender(attacker: CombatTrackerSlotController, defender: CombatTrackerSlotController) {
            if (atk.mode == AttackMode.Withering) {
                if (defender.isBattlegroupCheck.isSelected) {
                    defender.healthTrackController.addDamage(atk.amount, attacker.attackingDamageType)
                } else {
                    val result = incTextFieldNumber(-atk.amount, 9999, defender.initField)
                    if (attacker.isBattlegroupCheck.isSelected) {
                        if (result.incWasPossible && defender.initField.text.toInt() < 0) {
                            defender.healthTrackController.addDamage(-defender.initField.text.toInt(), attacker.attackingDamageType)
                            defender.initField.text = "0"
                        }
                    }
                }

                if (attacker.isBattlegroupCheck.isSelected) {

                } else {
                    val initGained = if (defender.isBattlegroupCheck.isSelected) {1} else {atk.amount+1}
                    incTextFieldNumber(initGained, 9999, attacker.initField)
                }
            } else {
                defender.healthTrackController.addDamage(atk.amount, attacker.attackingDamageType)
                if (attacker.isBattlegroupCheck.isSelected) {attacker.resetInit()}
            }
        }

        fun battlegroupAttack(attacker: CombatTrackerSlotController) {
            attacker.enemiesInRangeListSelector.targetItems.forEach {
               makeAttackOnDefender(attacker, slots.keys.getElementWithName(it) as CombatTrackerSlotController)
            }
        }

        if (attacker.isBattlegroupCheck.isSelected) {
            battlegroupAttack(attacker)
        } else {
            makeAttackOnDefender(attacker, defender)
        }


        val notificationTitle = if (atk.mode == AttackMode.Withering) {
            "Clang!"
        } else {
            when(attacker.attackingDamageType) {
                DamageType.Aggravated -> "Tssssssssss"
                DamageType.Bashing -> "Crunch!"
                DamageType.Lethal -> "Spurt!"
                else -> "Clang!"
            }
        }
        Notifications.create().title(notificationTitle).text("${atk.nameOfAttacker} attacked ${atk.nameOfDefender}, doing ${atk.amount} damage").showInformation()

    }

    fun loadCombatSession() {
        val combatantList = loadFileFromChooser<List<Combatant>>("Combat Session File", ".comsessh")
        combatantList.forEach { setCombatantToSlot(it, addCombatSlot().first) }
        refreshNameMenus()
    }

    fun saveCombatSession() {
        fun combatantListOf(list: List<CombatTrackerSlotController>) : List<Combatant> {
            var toReturn = listOf<Combatant>()
            list.forEach { toReturn += it.combatant }
            return toReturn
        }
        saveFileWithChooser<List<Combatant>>(combatantListOf(slots.keys.toList()),"Combat Session File", ".comsessh")
    }

    lateinit var slotsVBox: VBox
    val slots = mutableMapOf<CombatTrackerSlotController, Node>()
    lateinit var app: Application
}



