package com.blakwurm.anathama3.uiLogic.CombatTracker

import com.blakwurm.anathama3.character.DamageTaken
import com.blakwurm.anathama3.character.DamageType
import com.blakwurm.anathama3.character.HealthMod
import com.blakwurm.anathama3.getPointValue
import com.blakwurm.anathama3.with
import javafx.event.ActionEvent
import javafx.fxml.Initializable
import javafx.scene.control.Label
import javafx.scene.control.ProgressBar
import javafx.scene.control.ToggleGroup
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import java.net.URL
import java.util.*

/**
 * Created by Alex on 4/29/2016.
 */
class HealthTrackController : Initializable {
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        showDamage(trackView = healthTrackWidget, damageTaken = currentDamage, mods = currentHealthMod)// create the initial health track
        setupHealProgress()
    }

    lateinit var bonusBoxSelect: ToggleGroup
    lateinit var healthTrackWidget: TilePane

    lateinit var healProgressBar: ProgressBar
    lateinit var healProgressMessageLabel: Label
    private var hoursHealed: Int = 0 // counter value

    private val whatDamage: Int
        get() { // If remove is selected, remove damage. If nothing is selected
            // or "add" is selected, then add damage.
            if (shiftPressed) {return -1}
            else {return 1}
        }
    var shiftPressed = false
    var currentHealthMod = HealthMod() // Instantiate a blank/new one. If a new character is being built in the sheet, this is sufficient.
    var currentDamage = DamageTaken()
    private val currentHealthTrack : HealthTrack
        get() = createCharacterHealthTrack(currentHealthMod)

    val rollPenalty: Int
        get() = currentHealthTrack.penaltyFor(currentDamage)



    private fun checkForShiftPressed(mouseEvent: MouseEvent) {
        shiftPressed = mouseEvent.isShiftDown
    }
    fun addBashingDamage(mouseEvent: MouseEvent) = addDamageFromButton(mouseEvent, DamageType.Bashing)
    fun addAggroDamage(mouseEvent: MouseEvent) = addDamageFromButton(mouseEvent, DamageType.Aggravated)
    fun addLethalDamage(mouseEvent: MouseEvent) = addDamageFromButton(mouseEvent, DamageType.Lethal)

    private fun addDamageFromButton(mouseEvent: MouseEvent, type: DamageType) {
        checkForShiftPressed(mouseEvent)
        /*if (currentDamage.total <= currentHealthTrack.totalBoxes) {
            currentDamage = DamageTaken(
                    bashing = if (type.equals(DamageType.Bashing)) currentDamage.bashing + 1 else currentDamage.bashing,
                    lethal = if (type.equals(DamageType.Lethal)) currentDamage.lethal + 1 else currentDamage.lethal,
                    aggravated = if (type.equals(DamageType.Aggravated)) currentDamage.aggravated + 1 else currentDamage.aggravated
            )
        }*/
        addDamage(1, type)

    }

    fun addDamage(amount: Int, type: DamageType) {
        if (currentDamage.total <= currentHealthTrack.totalBoxes) {
            currentDamage = DamageTaken(
                    bashing = if (type.equals(DamageType.Bashing)) currentDamage.bashing + amount else currentDamage.bashing,
                    lethal = if (type.equals(DamageType.Lethal)) currentDamage.lethal + amount else currentDamage.lethal,
                    aggravated = if (type.equals(DamageType.Aggravated)) currentDamage.aggravated + amount else currentDamage.aggravated
            )
            resetHealingTimer()
            currentDamage = ensureBoundedHealth(currentDamage,currentHealthTrack)
        }
        showDamage()
    }


    fun addBonusBox(actionEvent: ActionEvent) {
        putBonusBox(1)
        setupHealProgress()
    }
    fun removeBonusBox(actionEvent: ActionEvent) {
        putBonusBox(-1)
        setupHealProgress()
    }
    private fun putBonusBox(damage: Int, bonusBoxValueToggle: ToggleGroup = bonusBoxSelect) {
        fun boxFor(i: Int) : Int {
            return if (i == bonusBoxValueToggle.getPointValue()) damage else 0
        }
        currentHealthMod = HealthMod(
                currentHealthMod.zero + boxFor(0),
                currentHealthMod.one + boxFor(1),
                currentHealthMod.two + boxFor(2),
                currentHealthMod.four + boxFor(3)
        )
        showDamage()
    }

    fun healHour(mouseEvent: MouseEvent) = healFor(1)
    fun healTwelveHour(mouseEvent: MouseEvent) = healFor(12)
    fun healDay(mouseEvent: MouseEvent) = healFor(24)
    fun healWeek(mouseEvent: MouseEvent) = healFor(24*7)

    private fun healFor(hours: Int) {

        this.hoursHealed += hours
        val newHealthData = processHealing(hoursHealed, currentHealthTrack, currentDamage)
        this.currentDamage = newHealthData.second
        this.hoursHealed = newHealthData.first
        if (currentDamage.total == 0) resetHealingTimer()

        //if (hoursHealed < 0) hoursHealed = 0
        setupHealProgress()
        showDamage()

    }

    private fun setupHealProgress() {
        fun makeDisplayValues(hoursHealed: Int, healthTrack: HealthTrack, damageTaken: DamageTaken) : Pair<String, Double> {
            fun formatTimeFor(totalHours: Int) : String{
                val days = totalHours / 24
                val hours = totalHours % 24
                return if (days > 0) "$days days and $hours hours" else if (hours > 0) "$hours hours" else ""
            }
            val type = typeForDamage(damageTaken)
            val reqHealing = hoursRequiredToHeal(healthTrack.penaltyFor(damageTaken), typeForDamage(damageTaken))
            return (if (damageTaken.total == 0) "You're perfectly healthy." else "You have ${formatTimeFor(reqHealing-hoursHealed)} before your next box of damage heals") to (hoursHealed.toDouble() / reqHealing.toDouble())
        }

        val displayValues = makeDisplayValues(hoursHealed, currentHealthTrack, currentDamage)
        healProgressMessageLabel.text = displayValues.first
        healProgressBar.progress = displayValues.second
    }



    private fun resetHealingTimer() {
        hoursHealed = 0
        setupHealProgress()
    }

    private fun ensureBoundedHealth(damageTaken: DamageTaken, healthTrack: HealthTrack) : DamageTaken {
        fun boundVal(i: Int, max: Int = healthTrack.totalBoxes) : Int {
            if (i < 0) return 0
            else if (i > max+1) return max+1
            else return i
        }
        return DamageTaken(bashing = boundVal(damageTaken.bashing), lethal = boundVal(damageTaken.lethal), aggravated = boundVal(damageTaken.aggravated))
    }






    public fun refreshTrack() {
        showDamage()
    }

    private fun showDamage(trackView: TilePane = healthTrackWidget, damageTaken: DamageTaken = currentDamage, mods: HealthMod = currentHealthMod) { // I'm a bit drunk, so this could probably be improved... a lot
        tailrec fun createHealthTrackWidgets(healthTrack: HealthTrack, list: List<VBox> = listOf(), i: Int = 0) : List<VBox> {
            if (i > healthTrack.totalBoxes) {
                return list
            } else {
                val vBox = VBox()
                    vBox.styleClass.add("health-track-vbox")
                val label = Label(healthTrack.boxLabelFor(i))
                    label.styleClass.add("health-track-label")
                    vBox.children.addAll(Pane(), label)
                return createHealthTrackWidgets(healthTrack, list + vBox, i + 1)
            }
        }


        val aggroStyle = "damage-region-aggro"
        val lethalStyle = "damage-region-lethal"
        val bashingStyle = "damage-region-bashing"
        val clearStyle = "damage-region-clear"


        /*
        Counters. Why not do it functionally? Because JavaFX is designed for Java, and as such it's a boatload
        easier just to play the iteration game the way that java loves to do it.
         */
        var bashCounter = 0
        var lethalCounter = 0
        var aggroCounter = 0


        /*
        This iterates through the list of health levels in the health track, and sets them to their
        appropriate damage box values for the provided DamageTaken. It's designed so that damage
        works like what is described in the Exalted book, where more severe damage pushes less severe
        damage down.
         */
        trackView.children.clear()
        trackView.children.addAll(createHealthTrackWidgets(createCharacterHealthTrack(mods)))
        trackView.children.forEach {

            it as VBox
            val box = it.children.first() as Pane
            box.styleClass.clear()
            box.styleClass.add(clearStyle)
            if (aggroCounter < damageTaken.aggravated) {
                aggroCounter++
                box.styleClass.clear()
                box.styleClass.add(aggroStyle)
            } else if (lethalCounter < damageTaken.lethal) {
                lethalCounter++
                box.styleClass.clear()
                box.styleClass.add(lethalStyle)
            } else if (bashCounter < damageTaken.bashing) {
                bashCounter++
                box.styleClass.clear()
                box.styleClass.add(bashingStyle)
            } else {

            }

        }

    }

}

private data class HealthTrack(val zero: Int = 1, val one: Int = 2, val two: Int = 2, val four: Int = 1){
    val totalBoxes : Int
            get() = zero + one + two + four
    fun boxLabelFor(i: Int) : String {
        val penalty = penaltyFor(i)
        return if (penalty == -999)"Inc"
        else penalty.toString()
    }
    fun penaltyFor(i: Int) : Int {
        return                    if (i < zero)  0
        else                if (i < zero + one) -1
        else          if (i < zero + one + two) -2
        else   if (i < zero + one + two + four) -4
        else                                  -999 // Incapacitate
    }
    fun penaltyFor(damageTaken: DamageTaken) : Int {
        return penaltyFor(damageTaken.total-1)
    }
}

fun hoursRequiredToHeal(boxMod: Int, damageType: DamageType) : Int {
    val isBashing = damageType.equals(DamageType.Bashing)
    return when (boxMod) {
        0 -> if (isBashing)  1 else 24
        -1 -> if (isBashing) 12 else 24*2
        -2 -> if (isBashing) 24 else 24*3
        -4 -> if (isBashing) 24*2 else 24*5
        -999 -> 24*7
        else -> 1
    }
}

private fun typeForDamage(damageTaken: DamageTaken) : DamageType {
    return when {
        damageTaken.bashing != 0 -> DamageType.Bashing
        damageTaken.lethal != 0 -> DamageType.Lethal
        damageTaken.aggravated != 0 -> DamageType.Aggravated
        else -> DamageType.None
    }
}

private fun processHealing(hoursHealed: Int, healthTrack: HealthTrack, damageTaken: DamageTaken) : Pair<Int, DamageTaken> {
    tailrec fun ph(healthData: Pair<Int, DamageTaken>) : Pair<Int, DamageTaken> {
        val type = typeForDamage(healthData.second)
        val reqHealing = hoursRequiredToHeal(healthTrack.penaltyFor(healthData.second), type)
        if (reqHealing > healthData.first) return healthData
        else {
            return ph((healthData.first - reqHealing) to DamageTaken(
                    bashing = if (type.equals(DamageType.Bashing)) healthData.second.bashing - 1 else healthData.second.bashing,
                    lethal = if (type.equals(DamageType.Lethal)) healthData.second.lethal - 1 else healthData.second.lethal,
                    aggravated = if (type.equals(DamageType.Aggravated)) healthData.second.aggravated - 1 else healthData.second.aggravated
            ))
        }
    }
    return ph(hoursHealed to damageTaken)
}

private fun createCharacterHealthTrack(mods: HealthMod) : HealthTrack {
    val defaultTrack = HealthTrack()
    return HealthTrack(
            zero = defaultTrack.zero + mods.zero,
            one = defaultTrack.one + mods.one,
            two = defaultTrack.two + mods.two,
            four = defaultTrack.four + mods.four
    )
}

private fun calculateRollPenaltyFor(healthTrack: HealthTrack, i: Int) : Int {
    val h = healthTrack // for code readability
    return                           if (i < h.zero) 0
    else                    if (i < h.zero + h.one) -1
    else            if (i < h.zero + h.one + h.two) -2
    else   if (i < h.zero + h.one + h.two + h.four) -4
    else                                            -999 // Incapacitate
}