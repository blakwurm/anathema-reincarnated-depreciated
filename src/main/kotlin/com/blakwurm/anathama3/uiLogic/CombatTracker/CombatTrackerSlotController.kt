package com.blakwurm.anathama3.uiLogic.CombatTracker

import com.blakwurm.anathama3.character.Combatant
import com.blakwurm.anathama3.character.DamageType
import com.blakwurm.anathama3.character.Named
import com.blakwurm.anathama3.character.nameList
import com.blakwurm.anathama3.loader.saveFileWithChooser
import com.blakwurm.anathama3.tryAsInt
import com.blakwurm.anathama3.uiLogic.CombatTracker.HealthTrackController
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import org.controlsfx.control.ListSelectionView
import org.controlsfx.control.PopOver
import org.controlsfx.control.Rating
import java.net.URL
import java.util.*

class CombatTrackerSlotController : Initializable, Comparable<CombatTrackerSlotController>, Named {


    override fun initialize(location: URL?, resources: ResourceBundle?) {
        addHealthTrack()
        resetInit()
        bashingRadio.isSelected = true
        popOver = PopOver(enemiesInRangeListSelector)
        popOver.title = "Enemies within range of $name"
        popOver.arrowLocation = PopOver.ArrowLocation.LEFT_TOP
        popOver.isDetachable = true
    }

    fun addHealthTrack() {
        healthListHBox.isFillHeight = true
        val sheetLoader = FXMLLoader()
        val track = sheetLoader.load<Node>(this.javaClass.getResource("/ui/healthtrack.fxml").openStream())
        healthTrackController = sheetLoader.getController()
        val trackContainer = VBox()
        trackContainer.isFillWidth = true
        trackContainer.children.addAll(
                //Separator(Orientation.HORIZONTAL),
                //TextFields.createClearableTextField(),
                track
                //Separator(Orientation.HORIZONTAL)
        )
        healthListHBox.children.addAll(trackContainer)
    }

    lateinit var trackerController: CombatTrackerController

    lateinit var nameField: TextField
    override val name: String
        get() = nameField.text
    lateinit var notesArea: TextArea

    lateinit var healthTrackController: HealthTrackController

    lateinit var healthListHBox: HBox

    lateinit var initField: TextField
    lateinit var personalEssenceField: TextField
    lateinit var periphEssenceField: TextField
    lateinit var maxPersonalField: TextField
    lateinit var maxPeriphField: TextField

    lateinit var tempWillpowerRating: Rating
    lateinit var maxWillpowerRating: Rating
    lateinit var willpowerBindCheck: CheckBox

    lateinit var damageTypeDone: ToggleGroup
    lateinit var aggroRadio: RadioButton
    lateinit var bashingRadio: RadioButton
    lateinit var lethalRadio: RadioButton

    val enemiesInRangeListSelector: ListSelectionView<String> = ListSelectionView()
    lateinit var editEnemiesButton: Button
    lateinit var popOver: PopOver

    fun editEnemiesInRange() {
        popOver.show(editEnemiesButton)


    }

    fun refreshEnemiesInRangeSelection(nameList: List<String> = trackerController.slots.keys.nameList()) {
        enemiesInRangeListSelector.sourceItems.clear()
        enemiesInRangeListSelector.sourceItems.addAll(nameList)
        enemiesInRangeListSelector.sourceItems.removeAll(this.name)
        enemiesInRangeListSelector.sourceItems.removeAll(enemiesInRangeListSelector.targetItems)
        if (!nameList.containsAll(enemiesInRangeListSelector.targetItems)) {
            var removeList = listOf<String>()
            enemiesInRangeListSelector.targetItems.forEach {
                if (!nameList.contains(it)) removeList += it
            }
            enemiesInRangeListSelector.targetItems.removeAll(removeList)
        }

    }

    fun nameFieldEdited() {
        trackerController.refreshNameMenus()
    }

    fun setDamageType(damageType: DamageType) {
        when (damageType) {
            DamageType.Aggravated -> aggroRadio.isSelected = true
            DamageType.Lethal -> lethalRadio.isSelected = true
            DamageType.Bashing -> bashingRadio.isSelected = true
            else -> bashingRadio.isSelected = true

        }
    }

    val attackingDamageType: DamageType
        get() {
            return when {
                aggroRadio.isSelected == true -> DamageType.Aggravated
                lethalRadio.isSelected == true -> DamageType.Lethal
                bashingRadio.isSelected == true -> DamageType.Bashing
                else -> DamageType.Bashing
            }
        }

    lateinit var isBattlegroupCheck: CheckBox

    fun decInit() = incTextFieldNumber(-1, 99, initField)
    fun incInit() = incTextFieldNumber(1, 99, initField)
    fun resetInit() = incTextFieldNumber(99999, 3, initField)

    fun incPersonalEss() = incTextFieldNumber(1, maxPersonalField.text, personalEssenceField)
    fun decPersonalEss() = incTextFieldNumber(-1, maxPersonalField.text, personalEssenceField)
    fun incPeriphEss() = incTextFieldNumber(1, maxPeriphField.text, periphEssenceField)
    fun decPeriphEss() = incTextFieldNumber(-1, maxPeriphField.text, periphEssenceField)

    fun respire() = incTextFieldNumber(incTextFieldNumber(5, maxPeriphField.text, periphEssenceField).amountRemaining, maxPersonalField.text, personalEssenceField)

    fun willpowerChanged() = if (willpowerBindCheck.isSelected) {
        ensureBoundedRating(tempWillpowerRating, maxWillpowerRating.rating)
    } else {}

    fun removeFromCombat() {
        trackerController.removeSlot(this)
    }

    fun removeAndSave() {
        saveCombatant()
        removeFromCombat()
    }

    val combatant: Combatant
        get() = Combatant(
                name = nameField.text,
                note = notesArea.text,
                healthMod = healthTrackController.currentHealthMod,
                damageTaken = healthTrackController.currentDamage,
                initiative = initField.text.tryAsInt().result,
                personalEssenceMax = maxPersonalField.text.tryAsInt().result,
                personalEssence = personalEssenceField.text.tryAsInt().result,
                periphEssenceMax = maxPeriphField.text.tryAsInt().result,
                periphEssence = periphEssenceField.text.tryAsInt().result,
                tempWillpower = tempWillpowerRating.rating.toInt(),
                willpowerMax = maxWillpowerRating.rating.toInt(),
                bindWillpower = willpowerBindCheck.isSelected,
                isBattlegroup = isBattlegroupCheck.isSelected,
                damageType = attackingDamageType,
                enemiesInRange = enemiesInRangeListSelector.targetItems
        )
    fun saveCombatant() = saveFileWithChooser(toSave = this.combatant, fileHint = "Combatant File", filetype = ".combatant"
    )

    override fun compareTo(other: CombatTrackerSlotController): Int {
        return other.initField.text.tryAsInt().result - this.initField.text.tryAsInt().result
    }
}