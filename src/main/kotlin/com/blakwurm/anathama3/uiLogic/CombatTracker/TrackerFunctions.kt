package com.blakwurm.anathama3.uiLogic.CombatTracker

import com.blakwurm.anathama3.OperationResult
import com.blakwurm.anathama3.result
import com.blakwurm.anathama3.tryAsInt
import javafx.scene.control.TextField
import org.controlsfx.control.Rating

fun incTextFieldNumber(incAmount: Int, max: String, field: TextField) : TextFieldIncResult {
    val result = max.tryAsInt()
    if (!result.wasSuccessful) return TextFieldIncResult(incWasPossible = false,
            resultMessage = "Max wasn't an Int. Exception: ${result.error.message}", overMaxValue = false,
            amountApplied = 0, amountRemaining = incAmount)
    return incTextFieldNumber(incAmount, result.result, field)
}

fun incTextFieldNumber(incAmount: Int, max: Int, field: TextField) : TextFieldIncResult {
    val fieldToIntResult = field.text.tryAsInt()
    if (!fieldToIntResult.wasSuccessful) return TextFieldIncResult(incWasPossible = false,
            resultMessage = "TextField wasn't holding a valid Int. Exception: ${fieldToIntResult.error.message}",
            overMaxValue = false, amountApplied = 0, amountRemaining = incAmount)
    val potentialNumber = fieldToIntResult.result + incAmount
    val overMax = potentialNumber > max
    val actualNumber = if (overMax) {max} else {potentialNumber}
    val amountApplied = if (overMax) {max - fieldToIntResult.result} else { incAmount }
    val amountLeft = incAmount - amountApplied
    field.text = actualNumber.toString()
    return TextFieldIncResult(incWasPossible = true, resultMessage = "Successfully cast to Int", overMaxValue = overMax,
            amountApplied = amountApplied, amountRemaining = amountLeft)
}

fun ensureBoundedRating(changedRating: Rating, bound: Double) : OperationResult<Rating> {
    if (changedRating.rating > bound) changedRating.rating = bound
    return true result changedRating
}

data class AttackResult(val attackConfirmed: Boolean, val nameOfAttacker: String, val nameOfDefender: String, val amount: Int, val mode: AttackMode)
enum class AttackMode() {Withering, Decisive}
data class TextFieldIncResult(val incWasPossible: Boolean = true, val resultMessage: String = "No error, result completed successfully",
                              val overMaxValue: Boolean = false, val amountApplied: Int, val amountRemaining: Int)
                            {fun print(): TextFieldIncResult { println(this); return this}}