package com.blakwurm.anathama3

import com.blakwurm.anathama3.character.*
import com.blakwurm.anathama3.loader.loadAndlaunchWindow
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.blakwurm.anathama3.uiLogic.LauncherController
import com.blakwurm.anathama3.loader.loadAndlaunchWindow
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File

/**
 * Created by Alex on 4/11/2016.
 */
fun main(args:Array<String>) {
    Application.launch(AnathemaApp::class.java,*args) // Standard way to launch JavaFX apps.
}

class AnathemaApp : Application() {


    /**
     * Entry poiont for the app. This sets the app's stylesheet to "modena_mod.css" and opens
     * launcher.fxml. The app continues to execute in LauncherController.
     */
    override fun start(primaryStage: Stage) {
        /*var loader = FXMLLoader()
        val mainScene = loader.load<Node>(this.javaClass.getResource("/ui/launcher.fxml").openStream())
        val controller = loader.getController<LauncherController>()
        controller.app = this

        val scene = Scene(mainScene as Parent, 1050.0, 600.0)

        scene.stylesheets.add("ui/solarStyle.css")

        primaryStage.setTitle("Launcher")
        primaryStage.setScene(scene)
        primaryStage.show()*///val chooser = FileChooser()
        //chooser.extensionFilters.addAll(makeFileChoosers("CSS Files", ".css"))
        //val stage = Stage()
        //val file = chooser.showOpenDialog(stage) ?: File("C:/Users/Alex/exalted/anathema3/src/main/resources/ui/modena_mod.css")
        //scene.stylesheets.add("file:///" + file.absolutePath.replace("\\", "/"))
        //Application.setUserAgentStylesheet("file:///C:/Users/Alex/exalted/anathema3/src/main/resources/ui/charmEntryStyle.css")
        setUserAgentStylesheet("ui/modena_mod.css")
        loadAndlaunchWindow<LauncherController>(application = this, fxmlName = "launcher.fxml", title = "Anathema Reincarnated", windowHeight = 400.0, windowWidth = 400.0)
        { app, load, subject, stage ->
            val controller = load.getController<LauncherController>()
            controller.app = this
            controller.stage = stage
            load
        }
    }

    override fun stop() {
        LifecycleCallbacks.stopCallbacks.forEach {
            try {
                it() // call the function
            } catch (e:Exception) {
                // Do nothing, because we're at the end of the program's lifecycle and there's not much we can do.
            }
        }
        super.stop()
    }
}

/*fun solarTest() {
    println("running!")
    val testSolar = getTestSolar()
    println("This new solar is $testSolar")
    val gson = GsonBuilder().setPrettyPrinting().create()
    val serializedTestSolar = gson.toJson(testSolar)
    println(serializedTestSolar)
    val testSolarFile = File("TestSolar.solar")
    testSolarFile.appendText(serializedTestSolar)
    println("" + gson.fromJson(serializedTestSolar, Solar::class.javaObjectType))
    val newTestSolarFile = File("TestSolar.solar")
    println(newTestSolarFile.readText())
    println(Gson().fromJson(newTestSolarFile.readText(), Solar::class.java))
}

fun getTestSolar() : Solar {
    return Solar(
            mortal = Mortal(
                    name = "Rajmael", player = "Alex",
                    attributes = Attribute.values().associate { it to 1 },
                    abilities = Ability.values().associate { it to 0 },
                    merits = listOf(
                            Merit(name = "Cult",
                                    description = "I have a large number of followers",
                                    possibleRatings = listOf(1, 3, 5))),
                    intimacies = listOf(Intimacy(type = IntimacyType.Principle, intensity = IntimacyIntensity.Defining, subject = "The Pope", description = "I really, really hate that guy")),
                    essenceLevel = 1,
                    healthMods = HealthMod()),
            anima = "A bright and shining display that stretches to the heavens",
            charms = listOf(),
            caste = SolarCaste.ECLIPSE)
}*/

/**
 * Contains a mutable list of functions. These functions are called at the end of the application lifecycle, before
 * the app closes. Each function is executed in a try block, and any resulting exception is not handled by the
 * catch block. Care should be taken when adding callbacks to this list, as any objects referenced will not be
 * dereferenced and garbage collected until the app closes.
 */
object LifecycleCallbacks {
    val stopCallbacks: MutableList<() -> Unit> = mutableListOf()
}