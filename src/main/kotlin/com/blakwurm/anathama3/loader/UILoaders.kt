package com.blakwurm.anathama3.loader

import com.blakwurm.anathama3.character.Solar
import com.blakwurm.anathama3.character.SolarSpecialization
import com.blakwurm.anathama3.uiLogic.LauncherController
import com.blakwurm.anathama3.makeFileChoosers
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.FileChooser
import javafx.stage.Modality
import javafx.stage.Stage
import java.io.File
import kotlin.concurrent.thread

/**
 * Initializes and launches a window, executing the user's lambda after loading the fxml but before applying it to
 * a scene and affixing css.
 */
inline fun <T> loadAndlaunchWindow(application: Application,
                               fxmlName: String = "didntspecify.fxml",
                               cssName: String = "modena_mod.css", // not currently supported, but code references it so I'm leaving it.
                               title: String = "Launcher",
                               windowWidth: Double = 1050.0,
                               windowHeight: Double = 600.0,
                               blocking : Boolean = false,
                               customManipulator: (app: Application, loader: FXMLLoader, subject: Node, stage: Stage) -> FXMLLoader)
        : Pair<Stage,T> {

    var loader = FXMLLoader() // We use a loader, because we need to get controllers from quite a few of the windows that we launch.
    val primaryStage = Stage()
    val mainNode = loader.load<Node>(application.javaClass.getResource("/ui/$fxmlName").openStream())
    primaryStage.initModality(Modality.NONE)

    // Where the client's custom code will run
    customManipulator(application, loader, mainNode, primaryStage)

    //
    val scene = Scene(mainNode as Parent, windowWidth, windowHeight)
    scene.stylesheets.add("ui/modena_mod.css") // For now, we stick to modena_mod.css. Eventually I'll reintroduce actually loading custom CSS
    //scene.stylesheets.add("ui/$cssName")

    // This commented-out block is for UI skinning. Some tools require that the stylesheet be external before refreshing (understandable).
    /*val chooser = FileChooser()
    chooser.extensionFilters.addAll(makeFileChoosers("CSS Files", ".css"))
    val stage = Stage()
    val file = chooser.showOpenDialog(stage) ?: File("C:/Users/Alex/exalted/anathema3/src/main/resources/ui/solarStyle.css")
    scene.stylesheets.add("file:///" + file.absolutePath.replace("\\", "/"))*/
    /*file:///C:/Users/Alex/exalted/anathema3/src/main/resources/ui/solarStyle.css*/ // <-- note- this does not work. It just gets bundled into the jar


    primaryStage.title = title
    primaryStage.scene = scene
    println("before launch")
    if (blocking) {primaryStage.showAndWait()} else {primaryStage.show()}

    println("after launch")

    return primaryStage to loader.getController()

}

inline fun <reified T> showDialog(app: Application,
                   fxmlName: String = "didntspecify.fxml",
               title: String = "Please Make a Selection",
               windowWidth: Double = 400.0,
               windowHeight: Double = 120.0,
                  customManip: (T) -> Unit) : T {
    val result = loadAndlaunchWindow<T>(app, fxmlName, title, "modena_mod.css", windowWidth, windowHeight, true) {
        app, load, subject, stage ->
        stage.initModality(Modality.APPLICATION_MODAL)
        println("Before customManip")
        customManip(load.getController<T>())
        println("after customManip")
        load
    }
    return result.second
}

fun doNothingLoadParam(app: Application, loader: FXMLLoader, subject: Node, stage: Stage) : FXMLLoader {
    return loader
}