package com.blakwurm.anathama3.loader

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.blakwurm.anathama3.makeFileChoosers
import javafx.stage.FileChooser
import javafx.stage.Stage
import org.apache.commons.io.IOUtils
import java.io.File
import java.io.InputStream

inline fun <T> saveFileWithChooser(toSave: T, fileHint: String, filetype: String) : File {

    val gson = GsonBuilder().setPrettyPrinting().create()
    val serializedList = gson.toJson(toSave)
    println(serializedList)
    val chooser = FileChooser()
    chooser.extensionFilters.addAll(makeFileChoosers(fileHint, filetype))
    val file = chooser.showSaveDialog(Stage())
    file.setWritable(true)
    file.writeText(serializedList)
    return file
}

inline fun <reified T:Any> loadFileFromChooser(fileHint: String, filetype: String) : T {
    val chooser = FileChooser()
    chooser.extensionFilters.addAll(makeFileChoosers(fileHint, filetype))
    val file = chooser.showOpenDialog(Stage())
    val objectString = file.readText()
    val gson = Gson()
    return gson.fromJson<T>(objectString)
}

fun loadLocalFile(path: String) : InputStream {
    return LoadDummy::class.java.getResourceAsStream(path)
}

fun loadLocalTextFile(path: String) : String {
    /*String theString = IOUtils.toString(inputStream, encoding); */
    val stream = loadLocalFile(path)
    return IOUtils.toString(stream, "UTF-8")
}

inline fun <reified T:Any> loadObjectFromLocalFile(path: String) : T {
    return Gson().fromJson<T>(loadLocalTextFile(path))
}

private class LoadDummy()