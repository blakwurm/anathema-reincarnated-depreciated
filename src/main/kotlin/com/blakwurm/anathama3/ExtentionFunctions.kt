package com.blakwurm.anathama3

import javafx.collections.ObservableList
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.control.ToggleGroup
import org.apache.commons.lang3.text.WordUtils
import org.controlsfx.control.textfield.TextFields

/**
 * Created by Alex on 4/11/2016.
 */
fun String.firstCap(): String {
    return (this.substring(0,1).toUpperCase() + this.substring(1).toLowerCase())
}

fun <T> ObservableList<T>.toList() : List<T> {
    val toReturn = mutableListOf<T>()
    this.forEach {
        toReturn.add(it)
    }
    return toReturn
}

fun ToggleGroup.getPointValue(starting: Int = 0) : Int {
    var indexCounter = 0
    this.toggles.forEach {
        if (!it.isSelected) {
            indexCounter++
        } else {
            return indexCounter
        }
    }
    return 0 // If nothing is selected, return 0
}

fun ToggleGroup.setPointValue(points:Int) {
    toggles.forEach { it.isSelected = false } // clear the previous setting
    var counter = 0 // rather then iterate over index numbers, it's easier to just keep a counter
    for (toggle in toggles) {
        counter++
        if (counter == points) {
            toggle.isSelected = true
            break
        }
    }
}

fun String.wrapAt(location: Int, newlineText: String) : String {
        var toReturn = WordUtils.wrap(this, location, "\n ", true)
        toReturn = toReturn.replace("\n", "\n$newlineText")
        return toReturn
}

fun <T> ListView<T>.removeSelected() = this.items.remove(this.selectionModel.selectedItem)


var <T> ListView<T>.selected : T
    set(value) = this.selectionModel.select(value)
    get() = this.selectionModel.selectedItem

///**
// * Creates a tuple of type [Pair] from this and [that].
// *
// * This can be useful for creating [Map] literals with less noise, for example:
// * @sample test.collections.MapTest.createUsingTo
// */
//infix fun <A, B> A.and(that: B): Pair<A, B> = Pair(this, that)

infix fun <A,B, C> Pair<A,B>.with(other: C) : Triple<A,B,C> {
    return Triple(this.first, this.second, other)
}

infix fun TextField.autocompleteWith(results: Collection<String>) {
    TextFields.bindAutoCompletion(this, results)
}


fun String.tryAsInt() : OperationResult<Int> {
    try {this.toInt()} catch (e: NumberFormatException) { false result 0 saying "Couldn't cast to Int" because e}
    return true result this.toInt() saying "Successfully cast to Int"
}