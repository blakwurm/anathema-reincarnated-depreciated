package com.blakwurm.anathama3

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.blakwurm.anathama3.character.SolarCharm
import javafx.event.ActionEvent
import javafx.scene.control.ToggleGroup
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File
import java.util.*


/**
 * Created by Alex on 4/8/2016.
 */

/*fun ToggleGroup.getPointValue() : Int {
    var toReturn = 0
    for (toggle in toggles) {
        toReturn++
        if (toggle.isSelected) break
    }
    return toReturn
}*/



fun getDotNotation(points: Int, dots: String = "") : String {
    return if (points < 1) {
        dots
    } else {
        getDotNotation(points - 1, dots + "•")
    }
}

fun makeFileChoosers(fileHint: String, filetype: String) : List<FileChooser.ExtensionFilter> {
    return listOf(
            FileChooser.ExtensionFilter(fileHint, "*$filetype"),
            FileChooser.ExtensionFilter("all", "*.*")
    )
}